//
//  CheckFileUrl.h
//  PianoClubHouse
//
//  Created by Gerard Turman on 10/31/15.
//  Copyright © 2015 kingcode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckFileUrl : NSObject
{
    NSURLConnection *connectionManager;
    NSURL *videoURL;
}

-(void)checkUrl:(NSURL*)url;

@end
