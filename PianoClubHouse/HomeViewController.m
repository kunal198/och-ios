//
//  HomeViewController.m
//  PianoClubHouse
//
//  Created by kingcode on 9/29/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "HomeViewController.h"
#import "AppDelegate.h"
#import "BrowseViewController.h"
#import "SearchViewController.h"
#import "LoginViewController.h"
#import "JSON.h"
#import "SerialObject.h"
#import "MyList.h"
#import "Common.h"
#import "SubmitRequstVC.h"


@interface HomeViewController ()
{
    NSDictionary *m_dicFilterKeys;
    NSMutableArray *m_serverCategories;
    NSMutableDictionary *categoryNameDic;
   int rate;
}

@end


@implementation HomeViewController

#define GET_FILTERKEY_URL  @"http://organclubhouse.com/api?option=get_filterkeys"
#define GET_CATEGORIES_URL  @"http://www.organclubhouse.com/api/?option=get_category_list"


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    rate = [[NSUserDefaults standardUserDefaults]integerForKey:@"rateUs"];

    
    if (rate == 5)
    {
        _view_ratings.layer.cornerRadius = 10;
        _view11.hidden = false;
        _view_rating_backView.hidden = false;
        
    }
    else
    {
        _view11.hidden = true;
        _view_rating_backView.hidden = true;
    }
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(appDelegate.appMode != PREMIUM/*!IsPremium()*/)    //Standard mode
    {
    }
    else
    {
       
    }
    self.scrollView.scrollEnabled = YES;
    [self loadCategoriesFromServer];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.m_lbUserName.text = [NSString stringWithString:appDelegate.g_UserName];
}

-(void)loadFilterKeysFromServer
{
    NSString *urlAsString = [NSString stringWithFormat:@"%@", GET_FILTERKEY_URL];
    NSLog(@"URL = %@", urlAsString);
    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"GET"];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:nil error: nil];
    if(data == nil)
    {
        NSLog(@"Connection or Download failed.");
        return;
    }
    
    NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Results ==== %@", responseString);
    
    m_dicFilterKeys = (NSDictionary*)[responseString JSONValue];
    
    
    
}

-(void)loadCategoriesFromServer
{
    m_serverCategories = [[NSMutableArray alloc]init];
    categoryNameDic = [[NSMutableDictionary alloc]init];
    
    
    NSString *urlAsString = [NSString stringWithFormat:@"%@", GET_CATEGORIES_URL];
    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"GET"];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:nil error: nil];
    if(data == nil)
    {
        NSLog(@"Connection or Download failed.");
        return;
    }
    
    NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSDictionary *dic1 = [[NSDictionary alloc]initWithObjectsAndKeys:@"Browse All Videos",@"NAME",@"0",@"ID",@"0",@"order",nil];
    NSArray *dic2 = (NSArray*)[responseString JSONValue];
    
    [m_serverCategories addObject:dic1];
    [m_serverCategories addObjectsFromArray:dic2];
    
    [self sortedArray:m_serverCategories];
    
   // UIStoryboard *mainStoryboard;
   
    if(IDIOM == IPAD)
    {
        
        int width = (self.view.frame.size.width - 10*4) /4;
        
        int y = self.view.frame.size.width/12;
        int p = 0;
        
        for(int i = 1; i<= m_serverCategories.count; i++){
            
            UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/10+p, y, width, width)];
            [button setBackgroundColor:Rgb2UIColor(25, 11, 35)];
            
            button.layer.borderWidth = 1.3;
            button.layer.borderColor = [[UIColor whiteColor] CGColor];
            button.layer.cornerRadius = 10;
            [button.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
            [button setTitle:[[m_serverCategories valueForKey:@"NAME"] objectAtIndex:i-1] forState:UIControlStateNormal];
            [button setTag:[[[m_serverCategories valueForKey:@"ID"] objectAtIndex:i-1] integerValue]];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
            button.titleLabel.textAlignment = NSTextAlignmentCenter;
            
            [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [categoryNameDic setObject:[[m_serverCategories valueForKey:@"NAME"] objectAtIndex:i-1] forKey:[[m_serverCategories valueForKey:@"ID"] objectAtIndex:i-1]];
            
            [self.scrollView addSubview:button];
            
            if(i % 3 == 0){
                y = y+width+30;
                p = 0;
            }
            else{
                p = p + width+30;
            }
        }
        [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, y+10)];

    }
    else
    {
        
        int width = (self.view.frame.size.width - 10*4) /3;
        
        int y = self.view.frame.size.width/8;
        int p = 0;
        
        for(int i = 1; i<= m_serverCategories.count; i++){
            
            UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(10+p, y, width, width)];
            [button setBackgroundColor:Rgb2UIColor(25, 11, 35)];
            
            button.layer.borderWidth = 1.3;
            button.layer.borderColor = [[UIColor whiteColor] CGColor];
            button.layer.cornerRadius = 10;
            [button.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
            [button setTitle:[[m_serverCategories valueForKey:@"NAME"] objectAtIndex:i-1] forState:UIControlStateNormal];
            [button setTag:[[[m_serverCategories valueForKey:@"ID"] objectAtIndex:i-1] integerValue]];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
            button.titleLabel.textAlignment = NSTextAlignmentCenter;
            
            [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [categoryNameDic setObject:[[m_serverCategories valueForKey:@"NAME"] objectAtIndex:i-1] forKey:[[m_serverCategories valueForKey:@"ID"] objectAtIndex:i-1]];
            
            [self.scrollView addSubview:button];
            
            if(i % 3 == 0){
                y = y+width+10;
                p = 0;
            }
            else{
                p = p + width+10;
            }

        }

        [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, y+10)];

    
       }
    
}



-(IBAction)buttonPressed:(id)sender{
    
    [self performSegueWithIdentifier:@"BrowsSegIdent" sender:sender];
    
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"searchSegue"])
    {
        SearchViewController *destViewController = segue.destinationViewController;
        destViewController.hidesBottomBarWhenPushed = YES;
    }
    else if([segue.identifier isEqualToString:@"backToLogin"])
    {
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.  viewControllers];
        [navigationArray removeAllObjects];
        LoginViewController *destViewController = segue.destinationViewController;
        destViewController.hidesBottomBarWhenPushed = YES;
        NSLog(@" -- backToLogin -- ");
    }
    
    else if ([segue.identifier isEqualToString:@"BrowsSegIdent"]){
        
        NSInteger i = [sender tag];
        BrowseViewController *destViewController = segue.destinationViewController;
        NSLog(@"%ld",(long)i);
        
        destViewController.m_nCategory  = [NSNumber numberWithInteger:i];
        destViewController.catgoryName = [categoryNameDic valueForKey:[NSString stringWithFormat:@"%ld",(long)[sender tag]]];
    }
    
    else if([segue.identifier isEqualToString:@"SubmitRequestSeg"])
    {
        SubmitRequstVC *destViewController = segue.destinationViewController;
        destViewController.hidesBottomBarWhenPushed = YES;
    }
    

}

-(void)sortedArray:(NSMutableArray*)array{
    
    bool swapped = TRUE;
    while (swapped){
        swapped = FALSE;
        for (int i=1; i<array.count;i++)
        {
            if ([[[array valueForKey:@"order"] objectAtIndex:(i-1)] intValue] > [[[array valueForKey:@"order"] objectAtIndex:(i)] intValue])
            {
                [array exchangeObjectAtIndex:(i-1) withObjectAtIndex:i];
                swapped = TRUE;NSLog(@"print2");
            }
            NSLog(@"print1");
        }
        NSLog(@"print");
    }
    
    
}
- (IBAction)rating_itunesLink:(id)sender
{
    [[NSUserDefaults standardUserDefaults]setInteger:555 forKey:@"rateUs"];
    self.view_rating_backView.hidden = true;
    self.view11.hidden = true;
    NSString *iTunesLink = @"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=1238735500&pageNumber=0&sortOrdering=1&type=Purple+Software&mt=8";         //  @"https://itunes.apple.com/in/app/pianoclubhouse-easy-piano-song-tutorial-lessons/id1238735500?mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

- (IBAction)rating_notNow:(id)sender
{
    _view11.hidden = true;
    _view_rating_backView.hidden = true;
    [[NSUserDefaults standardUserDefaults]setInteger:555 forKey:@"rateUs"];
}

@end
