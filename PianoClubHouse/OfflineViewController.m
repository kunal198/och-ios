//
//  OfflineViewController.m
//  PianoClubHouse
//
//  Created by Gerard Turman on 11/13/15.
//  Copyright © 2015 kingcode. All rights reserved.
//

#import "OfflineViewController.h"
#import "MyList.h"
#import "AppDelegate.h"
#import "Common.h"

@interface OfflineViewController ()
{
    BOOL m_bHasDownloads;
}

@end

@implementation OfflineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  //  [self.m_btnContinue setBackgroundImage:[UIImage imageNamed:@"button_back"] forState:UIControlStateNormal];
   // [self.m_btnContinue setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    MyList* g_list = [MyList getInstance];
    if([g_list.m_downloadedList count] == 0)
    {
        m_bHasDownloads = NO;
        [self.m_btnContinue setTitle:@"Check For Network" forState:UIControlStateNormal];
    }
    else
    {
        m_bHasDownloads = YES;
        [self.m_btnContinue setTitle:@"Continue To My Downloads" forState:UIControlStateNormal];
    }
}


- (IBAction)onBtnContinueClick:(id)sender
{
    if(m_bHasDownloads)
    {
        AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.appMode = PREMIUM;
        [self performSegueWithIdentifier:@"toDownloads" sender:self];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}
@end
