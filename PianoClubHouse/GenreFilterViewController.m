//
//  GenreFilterViewController.m
//  PianoClubHouse
//
//  Created by kingcode on 8/24/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "GenreFilterViewController.h"
#import "SearchViewController.h"
#import "FilterCell.h"
#import "Common.h"
#import "G_Functions.h"
#import "NSString+SBJSON.h"


#define GET_CATEGORIES_URL  @"http://www.organclubhouse.com/api/?option=get_category_list"
@interface GenreFilterViewController ()

@end

@implementation GenreFilterViewController

@synthesize m_tableView;
@synthesize m_arrGenres;
@synthesize m_dicGenres;
@synthesize m_navbar;
@synthesize m_arrSelected;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

   // self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"search_back"]];
    
    if(IDIOM == IPAD)
        m_navbar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar_ipad"]];
    else
        m_navbar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"browse_title"]];
    
    m_tableView.allowsSelection = NO;
    [self initFilters];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)initFilters
{
    NSString *urlAsString = [NSString stringWithFormat:@"%@", GET_CATEGORIES_URL];
    NSLog(@"URL = %@", urlAsString);
    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"GET"];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:nil error: nil];
    if(data == nil)
    {
        NSLog(@"Connection or Download failed.");
        return;
    }
    
    NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Results ==== %@", responseString);
    
    NSMutableArray *dic1 = (NSMutableArray*)[responseString JSONValue];
    [self sortedArray:dic1];
    
    m_dicGenres = [[NSMutableDictionary alloc]init];
    m_arrGenres = [[NSMutableArray alloc]init];
    
    for(int i =0 ;i<dic1.count;i++){
        
        NSString *name = [[dic1 valueForKey:@"NAME"] objectAtIndex:i];
        NSNumber *ID = [[dic1 valueForKey:@"ID"] objectAtIndex:i];
        
        [m_dicGenres setObject:ID forKey:name];
        [m_arrGenres addObject:name];
        
    }
    
    NSLog(@"%@",m_dicGenres);
    NSLog(@"%@",m_arrGenres);
    
    
//    
//    m_dicGenres = @{@"Popular Tutorials" : [NSNumber numberWithInt:1],
//                    @"Gospel Tutorials" : [NSNumber numberWithInt:6] ,
//                    @"Christmas Tutorials" : [NSNumber numberWithInt:11] ,
//                    @"Theory Tutorials" : [NSNumber numberWithInt:4],
//                    @"T.V. Theme Tutorials" : [NSNumber numberWithInt:8],
//                    @"Christian Tutorials" : [NSNumber numberWithInt:7],
//                    @"Technique Tutorials" : [NSNumber numberWithInt:5]};
//    
//    
//    
//      m_arrGenres = [[NSMutableArray alloc]initWithObjects:@"Popular Tutorials",
//                        @"Gospel Tutorials",
//                        @"Christmas Tutorials",
//                        @"Theory Tutorials",
//                        @"T.V. Theme Tutorials",
//                        @"Christian Tutorials",
//                        @"Technique Tutorials",
//                        nil];
//    
}

- (IBAction)onBackBtnClick:(id)sender
{
    [self getSelectedGenres];
    
    SearchViewController* pBackView = (SearchViewController*)[self backViewController];
    NSLog(@"%@",m_arrSelected);
    
    pBackView.m_genreFilters = m_arrSelected;
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIViewController *)backViewController
{
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers < 2)
        return nil;
    else
        return [self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 2];
}

#pragma marks - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return m_arrGenres.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return IDIOM == IPAD? 117.0f : 60.0f;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellIdentifier = @"genreCell";
    FilterCell* cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell)
    {
        cell = [[FilterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
/*
    if([G_Functions isPremiumMode])
        cell.filterName.textColor  = Rgb2UIColor(255,255,255);
    else
        cell.filterName.textColor  = Rgb2UIColor(0, 0, 0);//(128, 131, 140);
*/    
    NSString *strName = [m_arrGenres objectAtIndex:indexPath.row];
    cell.filterName.text = strName;
    
    NSNumber *i = [m_dicGenres objectForKey:strName];
    if(m_arrSelected && ([m_arrSelected indexOfObject:i] != NSNotFound))
        cell.m_toggleBtn.selected = YES;
    else
        cell.m_toggleBtn.selected = NO;
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    if ([m_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [m_tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([m_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [m_tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma marks - Button Actions

-(void)getSelectedGenres
{
    NSInteger sections = m_tableView.numberOfSections;
    for (int section = 0; section < sections; section++) {
        NSInteger rows =  [m_tableView numberOfRowsInSection:section];
        for (int row = 0; row < rows; row++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            FilterCell*cell = (FilterCell*)[self.m_tableView cellForRowAtIndexPath:indexPath];//**here, for those cells not in current screen, cell is nil**
            if(m_arrSelected == nil)
                m_arrSelected = [[NSMutableArray alloc]init];
            NSNumber *aNum = [m_dicGenres objectForKey:cell.filterName.text];
            if(cell.m_toggleBtn.selected)
            {
                if([m_arrSelected indexOfObject:aNum] == NSNotFound)
                    [m_arrSelected addObject:aNum];
            }
            else
            {
                if([m_arrSelected indexOfObject:aNum] != NSNotFound)
                    [m_arrSelected removeObject:aNum];
            }
        }
    }
}

- (IBAction)onResetBtnClick:(id)sender {
    
    NSInteger sections = m_tableView.numberOfSections;
    for (int section = 0; section < sections; section++) {
        NSInteger rows =  [m_tableView numberOfRowsInSection:section];
        for (int row = 0; row < rows; row++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            FilterCell*cell = (FilterCell*)[self.m_tableView cellForRowAtIndexPath:indexPath];//**here, for those cells not in current screen, cell is nil**
            [cell resetToggleBtn];
        }
    }
    
    [m_arrSelected removeAllObjects];
}
-(void)sortedArray:(NSMutableArray*)array{
    
    bool swapped = TRUE;
    while (swapped){
        swapped = FALSE;
        for (int i=1; i<array.count;i++)
        {
            if ([[[array valueForKey:@"order"] objectAtIndex:(i-1)] intValue] > [[[array valueForKey:@"order"] objectAtIndex:(i)] intValue])
            {
                [array exchangeObjectAtIndex:(i-1) withObjectAtIndex:i];
                swapped = TRUE;NSLog(@"print2");
            }
            NSLog(@"print1");
        }
        NSLog(@"print");
    }
    
    
}

@end
