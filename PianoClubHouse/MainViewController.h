//
//  MainViewController.h
//  PianoClubHouse
//
//  Created by kingcode on 9/29/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UITabBarController <UITabBarControllerDelegate>

@end
