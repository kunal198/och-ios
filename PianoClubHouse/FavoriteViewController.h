//
//  FavoriteViewController.h
//  PianoClubHouse
//
//  Created by kingcode on 8/16/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RRNCollapsableSectionTableViewController.h"
#import "BLMultiColorLoader.h"

@interface FavoriteViewController : RRNCollapsableTableViewController <UIAlertViewDelegate>

@property (strong, nonatomic) NSArray *menu;

@property (weak, nonatomic) IBOutlet UITableView *m_tableView;
@property (weak, nonatomic) IBOutlet UIView *m_navBar;
@property (weak, nonatomic) IBOutlet UILabel *m_sortType;
@property (weak, nonatomic) IBOutlet UILabel *m_lbCategory;
@property (weak, nonatomic) IBOutlet UIImageView *m_imgTableBack;
@property (weak, nonatomic) IBOutlet BLMultiColorLoader *m_loader;

- (IBAction)onSortBtnClick:(id)sender;
- (IBAction)onRemoveFavorite:(id)sender;
- (IBAction)onWatchOnlineBtnClick:(id)sender;
- (IBAction)onWatchOfflineBtnClick:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *m_ProgressPopup;
@property (weak, nonatomic) IBOutlet UIProgressView *m_downProgressView;
@property (weak, nonatomic) IBOutlet UILabel *m_lbDownProgress;
@property (weak, nonatomic) IBOutlet UILabel *m_lbDownloadingTitle;

@property (strong, nonatomic) NSURLConnection *connectionManager;
@property (strong, nonatomic) NSMutableData *downloadedMutableData;
@property (strong, nonatomic) NSURLResponse *urlResponse;

- (IBAction)onDownloadBtnClick:(UIButton *)sender;
- (IBAction)onDownloadCancel:(UIButton *)sender;

@end

