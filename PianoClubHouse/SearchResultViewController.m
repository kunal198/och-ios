//
//  BrowseViewController.m
//  PianoClubHouse
//
//  Created by kingcode on 8/16/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "SearchResultViewController.h"
#import "ListCell.h"
#import "LoadingView.h"
#import "JSON.h"
#import "SearchViewController.h"
#import "PreviewViewController.h"
#import "G_Functions.h"
#import "KxMenu.h"
#import "PostRequest.h"
#import "FakeModelBuilder.h"
#import "G_Filters.h"
#import "MyList.h"
#import "VideoData.h"
#import "UIButton+Tagged.h"
#import "Common.h"
#import "CheckFileUrl.h"
#import "Common.h"

@import MediaPlayer;

#define SERVER_URL  @"http://organclubhouse.com/api?option=get_video&category=%@"
#define GET_FILTERKEY_URL  @"http://organclubhouse.com/api?option=get_filterkeys"
#define SEARCH_REQUEST_URL  @"http://organclubhouse.com/api/search.php"


@interface SearchResultViewController ()
{
    NSArray* _videoData;
    LoadingView* loadingView;
    NSTimer* mainTimer;
    
    NSDictionary *m_dicFilterKeys;
    NSDictionary* m_dicInstructors;
    NSDictionary* m_dicKeys;
    NSDictionary* m_dicGenres;
    
    BOOL m_bTitleAscending;
    BOOL m_bArtistAscending;
    BOOL m_bDateAscending;
    
    NSInteger m_nCount;
    
    MPMoviePlayerViewController *videoPlayerView;
    NSURL *m_currentVideoURL;
    CheckFileUrl* checkObj;
    
    int nDownloadingCount;
    BOOL bAllowRotate;
    
    NSString* sDownloadFileName;
    NSInteger nDownloadIndex;
}

@end

@implementation SearchResultViewController

@synthesize m_nCategory;
@synthesize m_tableView;
@synthesize m_navBar;
@synthesize resultString;
@synthesize dicFilters;
@synthesize m_sortType;
@synthesize m_loader;


- (void)viewDidLoad {
    
    [super viewDidLoad];
    NSLog(@"%@",dicFilters);
    [self GetDataFromServer];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]];
    if(IDIOM == IPAD)
        m_navBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar_ipad"]];
    else
        m_navBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"browse_title"]];
    
    NSLog(@"Category ==== %@", m_nCategory);
    
    m_bTitleAscending = NO;
    m_bArtistAscending = NO;
    m_bDateAscending = NO;
    
    if(![G_Functions isPremiumMode])
        self.m_imgTableBack.image = [UIImage imageNamed:@"table_back_pub"];

    nDownloadingCount = 0;
    
    m_loader.lineWidth = 4.0;
    m_loader.colorArray = [NSArray arrayWithObjects:[UIColor redColor],
                           [UIColor purpleColor],
                           [UIColor greenColor],
                           [UIColor blueColor],
                           [UIColor whiteColor],nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadNotificationReceive:) name:@"DownloadingStartNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadNotificationReceive:) name:@"DownloadingEndNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceOrientationPortrait) name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];
}

- (BOOL)shouldAutorotate
{
    return bAllowRotate;
}

- (void)forceOrientationPortrait
{
    bAllowRotate = YES;
    
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInt:UIInterfaceOrientationPortrait] forKey:@"orientation"];
    
    bAllowRotate = NO;
}

-(void)downloadNotificationReceive:(NSNotification *) notification
{
    if([[notification name] isEqualToString:@"DownloadingStartNotification"])
    {
        if(nDownloadingCount == 0)
        {
            m_navBar.userInteractionEnabled = NO;
            //self.tabBarController.tabBar.userInteractionEnabled = NO;
        }
        
        nDownloadingCount++;
        NSLog (@"Successfully received DownloadingStartNotification!");
    }
    else if([[notification name] isEqualToString:@"DownloadingEndNotification"])
    {
        nDownloadingCount--;
        if(nDownloadingCount == 0)
        {
            m_navBar.userInteractionEnabled = YES;
            //self.tabBarController.tabBar.userInteractionEnabled = YES;
        }
        NSLog (@"Successfully received DownloadingEndNotification!");
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [m_tableView reloadData];
    self.tabBarController.tabBar.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(NSArray *)menu:(NSMutableArray*)array  {
    
    _menu = [FakeModelBuilder buildHomeListMenu:array];
    return _menu;
}

#pragma mark - RRNCollapsableTableView

-(NSString *)sectionHeaderNibName
{
    if([G_Functions isPremiumMode])
        return IDIOM == IPAD ? @"MenuSectionHeaderView_iPad" : @"MenuSectionHeaderView";
    else
        return @"MenuSectionHeaderView1";
}

-(NSArray *)model {
    return self.menu;
}

-(UITableView *)collapsableTableView {
    return m_tableView;
}

#pragma marks - Sort, Search

- (void)onBtnSearchClicked
{
    [self performSegueWithIdentifier:@"searchSegue" sender:self];
}

-(void)sortTableDataUsingSortKey:(NSString *)sortKey withAscending:(BOOL)isAscending
{
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:sortKey ascending:isAscending]];
    NSArray *arrTemp = [[NSArray alloc] initWithArray:[resultString sortedArrayUsingDescriptors:sortDescriptors]];
    [resultString count]?[resultString removeAllObjects]:NSLog(@"Datsource Table Not null");
    [resultString addObjectsFromArray:arrTemp];
    arrTemp = nil;
    
    [self menu:resultString];
    [m_tableView reloadData];
}

- (void)GetDataFromServer
{
    loadingView = nil;
    loadingView = [LoadingView loadingViewInView:self.view withTitle:NSLocalizedString(@"Loading...", nil)];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    mainTimer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(onStartTimer) userInfo:nil repeats:YES];
}


-(void) onStartTimer
{
    if(dicFilters == nil)
        [self LoadVideoList];
    else
        [self getSearchResult];
}

-(void)loadFilterKeysFromServer
{
    NSString *urlAsString = [NSString stringWithFormat:@"%@", GET_FILTERKEY_URL];
    NSLog(@"URL = %@", urlAsString);
    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"GET"];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:nil error: nil];
    if(data == nil)
    {
        NSLog(@"Connection or Download failed.");
        return;
    }
    NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Results ==== %@", responseString);
    
    m_dicFilterKeys = (NSDictionary*)[responseString JSONValue];
    NSLog(@"%@",m_dicFilterKeys);
    NSLog(@"%@",m_dicInstructors);
}

-(void) LoadVideoList
{
    
    [mainTimer invalidate];
    mainTimer = nil;
    
    NSString* strCategory = [m_nCategory stringValue];
    NSString *urlAsString = [NSString stringWithFormat:SERVER_URL, strCategory];
    NSLog(@"URL = %@", urlAsString);
    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"GET"];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:nil error: nil];
    if(data == nil)
    {
        NSLog(@"Connection or Download failed.");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OrganClubHouse" message:@"Connection or Download failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [loadingView removeView];
        loadingView = nil;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        return;
    }
    NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if([responseString isEqualToString:@"[]"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OrganClubHouse" message:@"No videos found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [loadingView removeView];
        loadingView = nil;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        return;
    }
    NSLog(@"Results ==== %@", responseString);
    
    
    resultString = (NSMutableArray*)[responseString JSONValue];
    
    m_nCount = [resultString count];
    self.m_sortType.text = [NSString stringWithFormat:@"%lu videos, sorted by publish date", (unsigned long)m_nCount];
    //m_videoNum.text = [NSString stringWithFormat:@"%lu videos", (unsigned long)[resultString count]];
    
    [self loadFilterKeysFromServer];        // Load Filter Keys
    
    
    [loadingView removeView];
    loadingView = nil;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    [self menu:resultString];
    [m_tableView reloadData];
    
}

-(void)getSearchResult
{
    [mainTimer invalidate];
    mainTimer = nil;
    
    NSString *jsonString;
    
    NSLog(@"%@",dicFilters);
    
    if ([NSJSONSerialization isValidJSONObject:dicFilters])
    {
        // Serialize the dictionary
        NSData* json = [NSJSONSerialization dataWithJSONObject:dicFilters options:/*NSJSONWritingPrettyPrinted*/0 error:nil];
        
        // If no errors, let's view the JSON
        if (json != nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"JSON: %@", jsonString);
            
            NSString* responseString = [PostRequest postDataToUrl:SEARCH_REQUEST_URL jsonString:jsonString];
            
            if(responseString == nil)
            {
                NSLog(@"Connection or Download failed.");
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OrganClubHouse" message:@"Connection or Download failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
                [loadingView removeView];
                loadingView = nil;
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                return;
            }
            
            if([responseString isEqualToString:@"[]"])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OrganClubHouse" message:@"No videos found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
                [loadingView removeView];
                loadingView = nil;
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                return;
            }
            
            resultString = (NSMutableArray*)[responseString JSONValue];
            
            [self menu:resultString];
            [m_tableView reloadData];
            m_nCount = [resultString count];
            self.m_sortType.text = [NSString stringWithFormat:@"%lu videos, sorted by publish date", (unsigned long)m_nCount];
            //m_videoNum.text = [NSString stringWithFormat:@"%lu videos", (unsigned long)[resultString count]];
            
        }
    }
    [loadingView removeView];
    loadingView = nil;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}


#pragma mark - UITableView

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    return IDIOM == IPAD? 86.0f : 45.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self model].count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellIdentifier = @"listCell";
    ListCell* cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(!cell)
    {
        cell = [[ListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"detail_bg"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0]];
    
    NSDictionary* dic = [resultString objectAtIndex:indexPath.section];
    NSString* videoTitle = [dic objectForKey:@"VideoTitle"]; videoTitle =  !videoTitle? @"": videoTitle;
    NSString* videoArtist = [dic objectForKey:@"VideoArtist"]; videoArtist =  !videoArtist? @"": videoArtist;
    NSString* videoID = [dic objectForKey:@"VideoID"];
    NSString* publishDate = [dic objectForKey:@"PublishDate"];  publishDate =  !publishDate? @"": publishDate;
    NSString* runtime = [dic objectForKey:@"VideoRuntime"];     runtime =  !runtime? @"": runtime;
    NSString* instructor = [dic objectForKey:@"InstructorID"];    instructor =  !instructor? @"": instructor;
    NSString* songkey = [dic objectForKey:@"SongKey"];    songkey =  !songkey? @"": songkey;
    NSString* mp4Name = [dic objectForKey:@"MP4Name"];    mp4Name =  !mp4Name? @"": mp4Name;
    NSString* previewID = [G_Functions getSafeString:[dic objectForKey:@"YouTubePreviewID"]];
    publishDate = [self convertDateFormat:publishDate];
    
    cell.video_id = videoID;        [cell refresh];
    cell.title = videoTitle;
    cell.artist = videoArtist;
    cell.preview_id = previewID;
    cell.m_lbVideoLength.text = [NSString stringWithFormat:@"%@ min", runtime];
    cell.m_lbPublishDate.text = publishDate;
    cell.m_lbSongKey.text = [self getSongKey:songkey];
    cell.m_lbInstructor.text = [self getInstructor:instructor];
    cell.mp4_name = mp4Name;
    
    cell.m_btnPreview.stringTag = [NSString stringWithFormat:@"%@:%@", previewID, videoTitle];//previewID;
    cell.m_btnWatch.stringTag = mp4Name;
    cell.m_btnWatchOffline.stringTag = mp4Name;
    cell.m_btnDownload.tag = indexPath.section;
    cell.m_lbl_Artist.text = videoArtist;
    return cell;
}

-(VideoData*)getVideoDataByID:(NSString*)video_id
{
    for(NSDictionary* dic in resultString)
    {
        if([video_id isEqualToString:[dic objectForKey:@"VideoID"]])
        {
            VideoData* pData = [[VideoData alloc] init];
            
            pData.video_id = video_id;
            pData.title = [dic objectForKey:@"VideoTitle"];
            pData.artist = [dic objectForKey:@"VideoArtist"];
            pData.previewID = [dic objectForKey:@"YouTubePreviewID"];
            pData.video_length = [dic objectForKey:@"VideoRuntime"];
            pData.publish_date = [dic objectForKey:@"PublishDate"];
            pData.instructor = [self getInstructor:[dic objectForKey:@"InstructorID"]];
            pData.song_key = [self getSongKey:[dic objectForKey:@"SongKey"]];
            pData.mp4_name = [dic objectForKey:@"MP4Name"];
            
            return pData;
        }
    }
    
    return nil;
}

-(NSString*)getSongKey:(NSString*)keyID
{
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *myNumber = [f numberFromString:keyID];
    
    G_Filters *gFilters = [G_Filters getInstance];
    NSString *songKey = [gFilters.m_keyFilters objectForKey:myNumber];
    return songKey;
}

-(NSString*)getInstructor:(NSString*)instructorID
{
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *myNumber = [f numberFromString:instructorID];
    
    int value = [myNumber intValue];
    myNumber = [NSNumber numberWithInt:value - 1];
    
    G_Filters *gFilters = [G_Filters getInstance];
    NSString *instructor = [gFilters.m_instructorFilters objectForKey:myNumber];
    return instructor;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    if ([m_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [m_tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([m_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [m_tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}


#pragma marks - DateFormatter

-(NSString*)convertDateFormat:(NSString*)str
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *date = [dateFormatter dateFromString: str];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yy"];
    
    NSString *convertedString = [dateFormatter stringFromDate:date];
    NSLog(@"Converted String : %@",convertedString);
    
    return convertedString;
}

#pragma marks - Go to Detail Page

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"searchSegue"]){
        SearchViewController *destViewController = segue.destinationViewController;
        destViewController.m_dicFilterKeys = m_dicFilterKeys;
    }
    else if ([segue.identifier isEqualToString:@"previewSegue"]) {
        PreviewViewController *destViewController = segue.destinationViewController;
        NSString* string = ((UIButton*)sender).stringTag;
        
        NSArray* list = [string componentsSeparatedByString:@":"];
        NSString* preview_id = [list objectAtIndex:0];
        NSString* sTitle = [list objectAtIndex:1];
        
        destViewController.m_sPreviewID = preview_id;
        destViewController.m_sTitle = sTitle;
    }
    
}

- (IBAction)onBackBtnClick:(id)sender
{
    self.tabBarController.tabBar.hidden = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onWatchOnlineBtnClick:(id)sender {
    
    NSString* mp4Name = ((UIButton*)sender).stringTag;
    m_currentVideoURL = [NSURL URLWithString:[NSString stringWithFormat:DOWNLOAD_URL, mp4Name]];
    
    NSLog(@"vurl %@",m_currentVideoURL);
    
    [m_loader startAnimation];
    [self disableBackground:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startPlay) name:@"ValidFileNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(alertVideo) name:@"InValidFileNotification" object:nil];
    
    if(checkObj == nil)
        checkObj = [[CheckFileUrl alloc] init];
    [checkObj checkUrl:m_currentVideoURL];

}

- (void)startPlay
{
    videoPlayerView = [[MPMoviePlayerViewController alloc] initWithContentURL:m_currentVideoURL];
    [self presentMoviePlayerViewControllerAnimated:videoPlayerView];
    [videoPlayerView.moviePlayer play];
    
    NSLog(@"starting");
    [m_loader stopAnimation];
    [self disableBackground:NO];
    
    NSLog(@"Removed 5");

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ValidFileNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"InValidFileNotification" object:nil];
}

- (void)alertVideo
{
    [m_loader stopAnimation];
    [self disableBackground:NO];
    
    NSLog(@"Removed 6");

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ValidFileNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"InValidFileNotification" object:nil];
    
    UIAlertView *confirmDlg=[[UIAlertView alloc]initWithTitle:@"OrganClubhouse" message:@"This file does not exist." delegate:self
                                            cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [confirmDlg show];
}

- (IBAction)onWatchOfflineBtnClick:(id)sender
{
    NSString* mp4Name  = ((UIButton*)sender).stringTag;
    NSURL *videoURL;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString* filePath = [documentsDirectory stringByAppendingPathComponent:mp4Name];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if(!fileExists)
    {
        NSLog(@"=====No file====");
        return;
    }
    
    videoURL = [NSURL fileURLWithPath:filePath];
    NSLog(@"vurl %@",videoURL);
    videoPlayerView = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
    [self presentMoviePlayerViewControllerAnimated:videoPlayerView];
    [videoPlayerView.moviePlayer play];
}

#pragma marks - Sort

- (IBAction)onSortBtnClick:(id)sender {
    [self showMenu:sender];
}

- (void)showMenu:(UIButton *)sender
{
    NSArray *menuItems =
    @[
      
      [KxMenuItem menuItem:@"Sort By Title"
                     image:nil
                    target:self
                    action:@selector(sortByTitle)],
      
      [KxMenuItem menuItem:@"Sort By Artist"
                     image:nil
                    target:self
                    action:@selector(sortByArtist)],
      
      [KxMenuItem menuItem:@"Sort By Date"
                     image:nil
                    target:self
                    action:@selector(sortByDate)],
      ];
    
    [KxMenu showMenuInView:self.view
                  fromRect:sender.frame
                 menuItems:menuItems];
}

-(void)sortByTitle
{
    m_bTitleAscending = !m_bTitleAscending;
    self.m_sortType.text = [NSString stringWithFormat:@"%lu videos, sorted by title", (unsigned long)m_nCount];
    [self sortTableDataUsingSortKey:@"VideoTitle" withAscending:m_bTitleAscending];
}

-(void)sortByArtist
{
    m_bArtistAscending = !m_bArtistAscending;
    self.m_sortType.text = [NSString stringWithFormat:@"%lu videos, sorted by artist", (unsigned long)m_nCount];
    [self sortTableDataUsingSortKey:@"VideoArtist" withAscending:m_bArtistAscending];
}

-(void)sortByDate
{
    m_bDateAscending = !m_bDateAscending;
    self.m_sortType.text = [NSString stringWithFormat:@"%lu videos, sorted by publish date", (unsigned long)m_nCount];
    [self sortTableDataUsingSortKey:@"PublishDate" withAscending:m_bDateAscending];
}

#define DISABLE_VALUE   0.7f
#define ENABLE_VALUE   1.0f

-(void)disableBackground:(BOOL)disable
{
    if(disable)
    {
        self.m_navBar.userInteractionEnabled = NO;
        self.m_tableView.userInteractionEnabled = NO;
        self.tabBarController.tabBar.userInteractionEnabled = NO;
        
        self.m_tableView.alpha = DISABLE_VALUE;
        self.m_navBar.alpha = DISABLE_VALUE;
    }
    else
    {
        self.m_navBar.userInteractionEnabled = YES;
        self.m_tableView.userInteractionEnabled = YES;
        self.tabBarController.tabBar.userInteractionEnabled = YES;
        
        self.m_tableView.alpha = ENABLE_VALUE;
        self.m_navBar.alpha = ENABLE_VALUE;
    }
}

#pragma mark - Download

- (IBAction)onDownloadBtnClick:(UIButton *)sender {
    
    self.downloadedMutableData = [[NSMutableData alloc] init];
    nDownloadIndex = sender.tag;
    sDownloadFileName = [[resultString objectAtIndex:nDownloadIndex] objectForKey:@"MP4Name"];
    self.m_lbDownloadingTitle.text = [[resultString objectAtIndex:nDownloadIndex] objectForKey:@"VideoTitle"];
    
    if(sDownloadFileName == nil)
    {
        NSLog(@"=== mp4 name is nil ===");
        UIAlertView *confirmDlg=[[UIAlertView alloc]initWithTitle:APP_LOGO message:@"This file does not exist." delegate:self
                                                cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [confirmDlg show];
        return;
    }
    
    self.m_ProgressPopup.hidden = NO;
    [self disableBackground:YES];
    
    self.m_downProgressView.progress = 0.0f;
    self.m_lbDownProgress.text = @"0%";
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:DOWNLOAD_URL, sDownloadFileName]]
                                                cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                            timeoutInterval:60.0];
    self.connectionManager = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"%lld", response.expectedContentLength);
    if(response.expectedContentLength == -1){
        [self cancelDownload];
        NSString* msgBody = @"Error downloading video. Please contact administrator.";
        UIAlertView *errorAlert=[[UIAlertView alloc]initWithTitle:APP_LOGO message:msgBody delegate:self
                                                cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [errorAlert show];
    }
    self.urlResponse = response;
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.downloadedMutableData appendData:data];
    self.m_downProgressView.progress = ((100.0/self.urlResponse.expectedContentLength)*self.downloadedMutableData.length)/100;
    if (self.m_downProgressView.progress == 1)
    {
        [self dismissDownloadPopup];
    }
    
    self.m_lbDownProgress.text = [NSString stringWithFormat:@"%.0f%%", ((100.0/self.urlResponse.expectedContentLength)*self.downloadedMutableData.length)];
    NSLog(@"%.0f%%", ((100.0/self.urlResponse.expectedContentLength)*self.downloadedMutableData.length));
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (self.downloadedMutableData)
    {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, sDownloadFileName];
        
        //saving is done on main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.downloadedMutableData writeToFile:filePath atomically:YES];
            NSLog(@"File Saved to %@!", filePath);
            
            [self saveToDownloadedList];
            [self.m_tableView reloadData];
        });
    }
    else
    {
        NSLog(@"Download Failed.");
    }
}

-(void)saveToDownloadedList
{
    MyList* g_list = [MyList getInstance];
    if(g_list.m_downloadedList == nil)
        g_list.m_downloadedList = [[NSMutableArray alloc] init];
    
    [g_list.m_downloadedList addObject:[self getVideoData:nDownloadIndex]];
}

-(VideoData*)getVideoData:(NSInteger)index
{
    NSDictionary* dic = [resultString objectAtIndex:index];
    
    VideoData* data = [[VideoData alloc] init];
    data.video_id = [dic objectForKey:@"VideoID"];
    data.title = [dic objectForKey:@"VideoTitle"];
    data.artist = [dic objectForKey:@"VideoArtist"];
    data.mp4_name = [dic objectForKey:@"MP4Name"];
    data.publish_date = [dic objectForKey:@"PublishDate"];
    data.previewID = [dic objectForKey:@"YouTubePreviewID"];
    data.instructor = [dic objectForKey:@"InstructorID"];
    data.song_key = [dic objectForKey:@"SongKey"];
    data.video_length = [dic objectForKey:@"VideoRuntime"];
    
    return data;
}

- (IBAction)onDownloadCancel:(UIButton *)sender {
    [self cancelDownload];
}

-(void)cancelDownload{
    [self.connectionManager cancel];
    self.connectionManager = nil;
    self.downloadedMutableData = nil;
    [self dismissDownloadPopup];
}

-(void)dismissDownloadPopup{
    self.m_ProgressPopup.hidden = YES;
    [self disableBackground:NO];
}

@end
