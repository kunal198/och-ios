//
//  FilterSettingViewController.h
//  PianoClubHouse
//
//  Created by kingcode on 8/18/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterSettingViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *m_tbFilterPanel;

- (IBAction)onResetBtnClick:(id)sender;
@property (weak, nonatomic) NSArray *m_dicFilterKeys;

@end
