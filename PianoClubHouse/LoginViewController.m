//
//  LoginViewController.m
//  PianoClubHouse
//
//  Created by kingcode on 9/5/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "LoginViewController.h"
#import "LoadingView.h"
#import "JSON.h"
#import "Common.h"
#import "AppDelegate.h"
#import "MainViewController.h"
#import "NSString+Extended.h"
#import "BrowseViewController.h"
#import "G_Functions.h"

@interface LoginViewController ()
{
    LoadingView* loadingView;
    NSTimer* mainTimer;
    
    NSString *sName;
    NSString *sPwd;
    BOOL m_bRemember;
}
@end

#define SIGNUP_URL  @"http://www.organclubhouse.com/members/signup.php"
#define LOGIN_URL  @"http://organclubhouse.com/api/login.php?name=%@&pwd=%@"

@implementation LoginViewController
NSString * appVersion;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appVersion = [[NSUserDefaults standardUserDefaults]objectForKey:@"updateDetail_key"];
    if ( [appVersion isEqualToString:@"update"]) {
        self.update_alert.hidden = false;
        
        if([[[NSUserDefaults standardUserDefaults]objectForKey:@"AppUnderMaintenance"] isEqualToString:@"YES"]){
        
            UILabel *l =[self.update_alert viewWithTag:1];
            [l setNumberOfLines:5];
            [l setText:@"We are sorry, but the app is currently undergoing routine maintenance. Please check back at a later time"];
            UIButton *B = [self.update_alert viewWithTag:2] ;
            [B setTitle:@"Check Later" forState:UIControlStateNormal];
            }
        else{
            UILabel *l =[self.update_alert viewWithTag:1];
            [l setText:@"Good news! A new version of the app is available."];
            
        }
   
    }
    self.update_alert.hidden = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    self.m_txtName.delegate = self;
    self.m_txtPwd.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"TestNotification"
                                               object:nil];
    self.loginPasswordView.frame = CGRectMake(self.view.frame.size.width, self.view.frame.origin.y, self.loginPasswordView.frame.size.width, self.loginPasswordView.frame.size.height);
    
}

-(void)viewWillAppear:(BOOL)animated{
    
  ///  self.loginPasswordView.frame = CGRectMake(CGRectGetWidth(self.welcomView.frame),20, CGRectGetWidth(self.loginPasswordView.frame), CGRectGetHeight(self.loginPasswordView.frame));
    
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    appVersion = [[NSUserDefaults standardUserDefaults]objectForKey:@"updateDetail_key"];
    self.update_alert.hidden = YES;
    if ( [appVersion isEqualToString:@"update"]) {
        self.update_alert.hidden = false;
        if([[[NSUserDefaults standardUserDefaults]objectForKey:@"AppUnderMaintenance"] isEqualToString:@"YES"]){
            UILabel *l =[self.update_alert viewWithTag:1];
            [l setNumberOfLines:5];
            [l setText:@"We are sorry, but the app is currently undergoing routine maintenance. Please check back at a later time"];
            UIButton *B = [self.update_alert viewWithTag:2] ;
            [B setTitle:@"Check Later" forState:UIControlStateNormal];
        }
        else{
            UILabel *l =[self.update_alert viewWithTag:1];
            [l setText:@"Good news! A new version of the app is available."];
        }
    }
    else{
        
    }
    
    if ([[notification name] isEqualToString:@"TestNotification"])
        NSLog (@"Successfully received the test notification!");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissKeyboard {
    [self.m_txtName resignFirstResponder];
    [self.m_txtPwd resignFirstResponder];
}

- (IBAction)update_val:(id)sender
{
   if([[[NSUserDefaults standardUserDefaults]objectForKey:@"AppUnderMaintenance"] isEqualToString:@"YES"]){
       exit(0);
   }
   else{
       NSString *iTunesLink = @"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=1238735500&pageNumber=0&sortOrdering=1&type=Purple+Software&mt=8";
       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
       //https://itunes.apple.com/in/app/organclubhouse-premium/id1063098413?mt=8
   }
}

- (IBAction)onBecomeBtnClick:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:SIGNUP_URL]];
}

- (IBAction)onLoginBtnClick:(id)sender {
    
    if([self.m_txtName.text length] == 0)
    {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"OrganClubHouse" message:@"Please enter your user name" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
        [alertView show];
        return;
    }
    
    if([self.m_txtPwd.text length] == 0)
    {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"OrganClubHouse" message:@"Please enter your password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
        [alertView show];
        return;
    }
    
    loadingView = nil;
    loadingView = [LoadingView loadingViewInView:self.view withTitle:NSLocalizedString(@"Log in...", nil)];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    mainTimer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(onStartTimer) userInfo:nil repeats:YES];
}

- (IBAction)onGuestBtnClick:(id)sender {
    
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.appMode = STANDARD;
    [self performSegueWithIdentifier:@"toTabViewSegue" sender:self];
}

- (IBAction)onRememberBtnClick:(id)sender
{
    UIButton* pButton = (UIButton*)sender;
    pButton.selected = !pButton.selected;
    
    if (pButton.selected) {
        m_bRemember = YES;
    }
}

-(void) onStartTimer
{
    [self login];
}

- (void)login
{
    [mainTimer invalidate];
    mainTimer = nil;
    
    [self.m_txtName resignFirstResponder];
    [self.m_txtPwd resignFirstResponder];
    
    sName = [[self.m_txtName text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    sPwd = [[self.m_txtPwd text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];  
    
    if ([sName length] == 0 || [sPwd length] == 0)
    {
        return;
    }
    
    NSString *urlAsString = [NSString stringWithFormat:LOGIN_URL, sName, [sPwd urlencode]];
    NSLog(@"URL = %@", urlAsString);
    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"GET"];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:nil error: nil];
    NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSLog(@"login response ==== %@", responseString);
    [self setAppMode:responseString];
    //NSDictionary *resultString = (NSDictionary*)[responseString JSONValue];
    
    [loadingView removeView];
    loadingView = nil;
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)setAppMode:(NSString*)string
{
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSLog(@"%@",string);
    
    NSArray* list = [string componentsSeparatedByString:@":"];
    NSString* previlige = [list objectAtIndex:0];
    if([list count] > 1)
    {
        appDelegate.g_UserName = [[NSString alloc] initWithString:[list objectAtIndex:1]];
        appDelegate.g_Email = [[NSString alloc] initWithString:[list objectAtIndex:2]];
    }
    
    if([previlige isEqualToString:PREMIUM_USER]){

        appDelegate.appMode = PREMIUM;
        [self performSegueWithIdentifier:@"toTabViewSegue" sender:self];
        
        if(m_bRemember)
           [self savePassword];
        return;
    }
    else if([previlige isEqualToString:STANDARD_USER]){
        
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"OrganClubHouse" message:@"You must be a Premium Member to access this area." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
    }
    else if([previlige isEqualToString:LOGIN_FAIL]){   //login fail
        
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"OrganClubHouse" message:@"Username or password are not correct." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
    }
    else
        NSLog(@"Something wrong: resultString === %@", string);
    appDelegate.appMode = STANDARD;

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if(textField == self.m_txtName) {
        [self.m_txtPwd becomeFirstResponder];
    } else if (textField == self.m_txtPwd) {
        [textField resignFirstResponder];
    }
    
    return NO;
}

-(void)savePassword
{
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [[NSUserDefaults standardUserDefaults] setValue:appDelegate.g_UserName forKey:@"FullName"];
    [[NSUserDefaults standardUserDefaults] setValue:appDelegate.g_Email forKey:@"Email"];
    [[NSUserDefaults standardUserDefaults] setValue:sName forKey:@"Name"];
    [[NSUserDefaults standardUserDefaults] setValue:sPwd forKey:@"PWd"];
    
    NSString *date_String=[self getUTCFormateDate:[NSDate date]];
    [[NSUserDefaults standardUserDefaults] setValue:date_String forKey:@"Date"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString *)getUTCFormateDate:(NSDate *)localDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:localDate];
    return dateString;
}

- (IBAction)backfFromLoginPasswordView:(id)sender {
    [UIView transitionWithView:self.loginPasswordView
                      duration:0.4
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    animations:^{
                        self.loginPasswordView.frame = CGRectMake(CGRectGetWidth(self.welcomView.frame),20, CGRectGetWidth(self.loginPasswordView.frame), CGRectGetHeight(self.loginPasswordView.frame));
                    }
                    completion:NULL];
    
}


- (IBAction)loginAsPremiumMember:(id)sender {
    
    [UIView transitionWithView:self.loginPasswordView
                      duration:0.4
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    animations:^{
                        self.loginPasswordView.frame = CGRectMake(0, 20, CGRectGetWidth(self.loginPasswordView.frame), CGRectGetHeight(self.loginPasswordView.frame));
        }
                    completion:NULL];
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isGuestUser"];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    if([segue.identifier isEqualToString:@"GuestUserSeg"]){
        
        BrowseViewController *browseViewController = [segue destinationViewController];
        [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isGuestUser"];
        [G_Functions IsGuestMode];
    }
    
}


@end
