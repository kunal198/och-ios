//
//  G_Functions.m
//  PianoClubHouse
//
//  Created by kingcode on 10/17/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "G_Functions.h"
#import "AppDelegate.h"
#import "Common.h"
#import "MyList.h"

@implementation G_Functions

+(BOOL)isPremiumMode
{
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.appMode == PREMIUM)
        return YES;
    else
        return NO;
}

+(BOOL)IsGuestMode{
    
   if([[[NSUserDefaults standardUserDefaults] valueForKey:@"isGuestUser"] isEqualToString:@"YES"])
        return YES;
    else
        return NO;
    
}


+(NSString*)getSafeString:(NSString*)str
{
    if([str isKindOfClass:[NSNull class]])
    {
        NSLog(@"something is null");
        return @"";
    }
    else
        return str;
}

+(void)saveToPurchasedList:(VideoData*)data
{
    MyList* g_list = [MyList getInstance];
    if(g_list.m_purchasedList == nil)
        g_list.m_purchasedList = [[NSMutableArray alloc] init];
    [g_list.m_purchasedList addObject:data];
    
    NSLog(@"%@ video saved to purchased list", data.video_id);
}

+(uint64_t)getFreeDiskspace
{
    uint64_t totalSpace = 0;
    uint64_t totalFreeSpace = 0;
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary)
    {
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalSpace = [fileSystemSizeInBytes unsignedLongLongValue];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
        NSLog(@"Memory Capacity of %llu MiB with %llu MiB Free memory available.", ((totalSpace/1024ll)/1024ll), ((totalFreeSpace/1024ll)/1024ll));
    }
    else
    {
        NSLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    
    return totalFreeSpace;
}

@end
