//
//  PostRequest.h
//  PianoClubHouse
//
//  Created by kingcode on 8/24/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostRequest : NSObject

+(NSString *)postDataToUrl:(NSString*)urlString jsonString:(NSString*)jsonString;

@end
