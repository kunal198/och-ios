//
//  UIButton+Tagged.m
//  PianoClubHouse
//
//  Created by kingcode on 10/14/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "UIButton+Tagged.h"
#import <objc/runtime.h>

static const void *tagKey = &tagKey;

@implementation UIButton (Tagged)

- (void)setStringTag:(NSString *)tag
{
    objc_setAssociatedObject(self, tagKey, tag, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)stringTag
{
    return objc_getAssociatedObject(self, tagKey);
}

@end
