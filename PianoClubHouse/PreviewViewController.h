//
//  PreviewViewController.h
//  PianoClubHouse
//
//  Created by kingcode on 8/19/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviewViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *m_pWebView;
@property (weak, nonatomic) IBOutlet UILabel *m_lbTitle;

@property (weak, nonatomic) NSString *m_sPreviewID;
@property (weak, nonatomic) NSString *m_sTitle;

@property (weak, nonatomic) IBOutlet UIView *m_navbar;
- (IBAction)onBackBtnClick:(id)sender;

@end
