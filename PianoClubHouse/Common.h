//
//  Common.h
//  PianoClubHouse
//
//  Created by kingcode on 8/24/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#ifndef PianoClubHouse_Common_h
#define PianoClubHouse_Common_h

#define Rgb2UIColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]
#define FAVORITE    @"favorite"
#define PURCHASED   @"purchased"
#define DOWNLOADED  @"downloaded"

#define MESSAGE_TITLE @"Message"

#define DOWNLOAD_URL @"https://s3.amazonaws.com/pchmp4/%@"
#define ProductID @"com.pianoclubhouse.videotutorials"

#define CONTACT_EMAIL   @"info@organclubhouse.com"

#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad

#define APP_LOGO   @"OrganClubHouse"

#import "AppDelegate.h"

typedef enum
{
   PREMIUM = 1,
    STANDARD
}APP_MODE;

#define PREMIUM_USER    @"premium"
#define STANDARD_USER    @"standard"
#define LOGIN_FAIL    @"login_fail"

//BOOL (^IsPremium)(void) =
//^{
//    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    if(appDelegate.appMode == PREMIUM)
//        return YES;
//    else
//        return NO;
//};

#endif
