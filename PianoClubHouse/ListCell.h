//
//  ListCell.h
//  PianoClubHouse
//
//  Created by kingcode on 10/9/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListCell : UITableViewCell <NSURLConnectionDataDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) NSString *video_id;
@property (weak, nonatomic) NSString *title;
@property (weak, nonatomic) NSString *artist;
@property (weak, nonatomic) NSString *mp4_name;
@property (weak, nonatomic) NSString *preview_id;

@property (weak, nonatomic) IBOutlet UILabel *m_lbVideoLength;
@property (weak, nonatomic) IBOutlet UILabel *m_lbSongKey;
@property (weak, nonatomic) IBOutlet UILabel *m_lbInstructor;
@property (weak, nonatomic) IBOutlet UILabel *m_lbPublishDate;

@property (weak, nonatomic) IBOutlet UIButton *m_btnSelectedFav;
@property (weak, nonatomic) IBOutlet UIButton *m_btnUnSelectedFav;
@property (weak, nonatomic) IBOutlet UIButton *m_btnDownload;
@property (weak, nonatomic) IBOutlet UIButton *m_btnWatch;
@property (weak, nonatomic) IBOutlet UIButton *m_btnWatchOffline;
@property (weak, nonatomic) IBOutlet UIButton *m_deleteBtn;

@property (weak, nonatomic) IBOutlet UILabel *m_lbDownloaded;
@property (weak, nonatomic) IBOutlet UIButton *m_btnPreview;
@property (weak, nonatomic) IBOutlet UIButton *m_btnPurchase;

@property (strong, nonatomic) IBOutlet UILabel *m_lbl_Artist;

- (IBAction)onFavoriteBtnClick:(id)sender;
- (IBAction)onDeleteBtnClick:(id)sender;
- (IBAction)onPurchaseBtnClick:(id)sender;

-(void)refresh;
-(void)refreshFavorites;
-(void)saveToPurchasedList;

@end
