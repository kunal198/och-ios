//
//  KeyFilterViewController.h
//  PianoClubHouse
//
//  Created by kingcode on 8/22/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeyFilterViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *m_navbar;
@property (strong, nonatomic) NSMutableArray *m_arrSelected;
@property (strong, nonatomic) NSMutableArray *m_selectedKeys;
@property (weak, nonatomic) IBOutlet UIButton *m_btnReset;

- (IBAction)onBackBtnClick:(id)sender;
- (IBAction)onFilter:(id)sender;
- (IBAction)onResetBtnClick:(id)sender;

@end
