//
//  MenuSectionHeaderView.m
//  Example
//
//  Created by Robert Nash on 08/09/2015.
//  Copyright (c) 2015 Robert Nash. All rights reserved.
//

#import "MenuSectionHeaderView.h"
#import "Common.h"
#define DEGREES_TO_RADIANS(x) (M_PI * (x) / 180.0)

@interface MenuSectionHeaderView ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end

@implementation MenuSectionHeaderView {
    BOOL isRotating;
}

@synthesize interactionDelegate = _interactionDelegate;

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
  
    [self.interactionDelegate userTapped:self];

}

-(void)openAnimated:(BOOL)animated {
    
    if (animated && !isRotating) {
        
        isRotating = YES;
        
        [UIView animateWithDuration:0.2 delay:0.0 options: UIViewAnimationOptionAllowUserInteraction |UIViewAnimationOptionCurveLinear animations:^{
            
            NSString *imgName = IDIOM == IPAD ? @"row_downHighlighted_ipad" : @"row_downHighlighted";
            self.imageView.image = [UIImage imageNamed:imgName];
            self.imageView.transform = CGAffineTransformIdentity;
            
        } completion:^(BOOL finished) {
            isRotating = NO;
        }];
        
    } else {
        
        NSString *imgName = IDIOM == IPAD ? @"row_downHighlighted_ipad" : @"row_downHighlighted";
        self.imageView.image = [UIImage imageNamed:imgName];
        self.imageView.transform = CGAffineTransformIdentity;
        [self.layer removeAllAnimations];
        isRotating = NO;
    }
}

-(void)closeAnimated:(BOOL)animated {
    
    if (animated && !isRotating) {
        
        isRotating = YES;
        
        [UIView animateWithDuration:0.2 delay:0.0 options: UIViewAnimationOptionAllowUserInteraction |UIViewAnimationOptionCurveLinear animations:^{
            
            NSString *imgName = IDIOM == IPAD ? @"row_down_iPad" : @"row_down";
            self.imageView.image = [UIImage imageNamed:imgName];
            self.imageView.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90.0f));
        
        } completion:^(BOOL finished) {
            isRotating = NO;
        }];
        
    } else {
        
        NSString *imgName = IDIOM == IPAD ? @"row_down_iPad" : @"row_down";
        self.imageView.image = [UIImage imageNamed:imgName];
        self.imageView.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90.0f));
        [self.layer removeAllAnimations];
        isRotating = NO;
    }
}

@end
