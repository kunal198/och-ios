//
//  AppDelegate.m
//  PianoClubHouse
//
//  Created by kingcode on 8/16/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "AppDelegate.h"
#import "MyList.h"
#import "SerialObject.h"
#import "Common.h"
#import "MKStoreManager.h"
#import "PreviewViewController.h"
#import "G_Filters.h"
#import <OneSignal/OneSignal.h>
#import <MediaPlayer/MediaPlayer.h>
#import "Reachability.h"
#import "WarningViewController.h"
#import "IQKeyboardManager.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "NSString+SBJSON.h"

@import Firebase;
@interface AppDelegate ()

@end

@implementation AppDelegate
 NSString * updateDetail;
 int rate;

@synthesize appMode;
@synthesize g_UserName;
@synthesize g_Email;
@synthesize previousTabIndex;
@synthesize currentTabIndex;
@synthesize previousScreen;
@synthesize m_arrSelectedInstructors;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
   
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"rateUs"]!= nil)
    {
            rate = [[NSUserDefaults standardUserDefaults]integerForKey:@"rateUs"];
            if (rate == 555)
            {
                // [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"rateUs"];
            }
            else
            {
                if (rate == 5)
                {
                    
                }
                else
                {
                    rate+=1;
                    [[NSUserDefaults standardUserDefaults]setInteger:rate forKey:@"rateUs"];
                }
            }
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"rateUs"];
    }
    
    
    int valueToSave = 0;
    [[NSUserDefaults standardUserDefaults] setInteger:valueToSave forKey:@"VideoToWatch" ];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Use Firebase library to configure APIs
    [FIRApp configure];
    // Initialize Google Mobile Ads SDK
    [GADMobileAds configureWithApplicationID:@"ca-app-pub-6153266407346561~9040045530"];
    
    // Override point for customization after application launch.
    MyList* g_list = [MyList getInstance];
    g_list.m_favoriteList = [NSMutableArray arrayWithArray:[SerialObject readFromUserDefaults:FAVORITE]];
    g_list.m_purchasedList = [NSMutableArray arrayWithArray:[SerialObject readFromUserDefaults:PURCHASED]];
    g_list.m_downloadedList = [NSMutableArray arrayWithArray:[SerialObject readFromUserDefaults:DOWNLOADED]];
    m_arrSelectedInstructors = [NSMutableArray new];
    
    [[MKStoreManager sharedManager] requestProductData];
    sleep(1);
    [[UIApplication sharedApplication] setStatusBarHidden:NO
                                            withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self initGlobalFilters];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
    
    [OneSignal initWithLaunchOptions:launchOptions appId:@"c85729ac-95fa-4f65-b66b-dc1e6e68528e" handleNotificationReceived:^(OSNotification *notification) {
        NSLog(@"Received Notification - %@ - %@", notification.payload.notificationID, notification.payload.title);
    } handleNotificationAction:^(OSNotificationOpenedResult *result) {
        NSLog(@"Tapped.........");
    } settings:nil];
    
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
       
    }
    else{
    
        NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
        NSString* appID = infoDictionary[@"CFBundleIdentifier"];
        NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.organclubhouse.com/api/?option=get_version"]];
       
        NSData* data = [NSData dataWithContentsOfURL:url];
        NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
        NSString* maintenance_mode = lookup[@"maintaince_mode"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"AppUnderMaintenance"];
    
        if([maintenance_mode isEqualToString:@"on"]){
            updateDetail = @"update";
            [[NSUserDefaults standardUserDefaults]setObject:updateDetail forKey:@"updateDetail_key"];
            [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"AppUnderMaintenance"];
            
            [self topViewController];
        
        }
        else{
            
            NSString* appStoreVersion = lookup[@"ios_version_id"];//[0][@"version_id"];
            NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
  
            if (![appStoreVersion isEqualToString:currentVersion]){
                NSLog(@"Need to update [%@ != %@]", appStoreVersion, currentVersion);
                updateDetail = @"update";
                [[NSUserDefaults standardUserDefaults]setObject:updateDetail forKey:@"updateDetail_key"];
                
                [self topViewController];
                
                return YES;
            }
            else if ([appStoreVersion isEqualToString:currentVersion]){
                updateDetail = nil;
                [[NSUserDefaults standardUserDefaults]setObject:updateDetail forKey:@"updateDetail_key"];
                return YES;
            }
            
            
        }
    }
    return YES;
}

//   NSString *version = [NSString stringWithFormat:@"Version %@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
//    NSLog(@" version :%@", version);
//    NSString *latestVersion = @"2.0";
//    if ([version isEqualToString:latestVersion]) {
//         vrsn = @"0";
//    }
//    else
//    {
//        vrsn = @"update";
//    }
//
//    return YES;

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    MyList* g_list = [MyList getInstance];
    [SerialObject writeToUserDefaults:FAVORITE withArray:g_list.m_favoriteList];
    [SerialObject writeToUserDefaults:PURCHASED withArray:g_list.m_purchasedList];
    [SerialObject writeToUserDefaults:DOWNLOADED withArray:g_list.m_downloadedList];
}

- (BOOL)applicationWillEnterForeground:(UIApplication *)application {
    
    NSLog(@"update here");
    
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        
    }
    else{
    
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString* appID = infoDictionary[@"CFBundleIdentifier"];
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.organclubhouse.com/api/?option=get_version"]];
    NSData* data = [NSData dataWithContentsOfURL:url];
    NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    NSString* maintenance_mode = lookup[@"maintaince_mode"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"AppUnderMaintenance"];
    
    if([maintenance_mode isEqualToString:@"on"]){
        updateDetail = @"update";
        [[NSUserDefaults standardUserDefaults]setObject:updateDetail forKey:@"updateDetail_key"];
        [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"AppUnderMaintenance"];
        
        [self topViewController];
        
    }
    else{
    
        NSString* appStoreVersion = lookup[@"ios_version_id"];//[0][@"version_id"];
        NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
    
        if (![appStoreVersion isEqualToString:currentVersion]){
            NSLog(@"Need to update [%@ != %@]", appStoreVersion, currentVersion);
            updateDetail = @"update";
            [[NSUserDefaults standardUserDefaults]setObject:updateDetail forKey:@"updateDetail_key"];
            [self topViewController];
        }
        else if ([appStoreVersion isEqualToString:currentVersion]){
            updateDetail = nil;
            [[NSUserDefaults standardUserDefaults]setObject:updateDetail forKey:@"updateDetail_key"];
           
        }
    
    }
        
    }
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"c"
     object:self];
    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    NSLog(@"wrong selection....????");
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

static NSString * const VIDEO_CONTROLLER_CLASS_NAME_IOS7 = @"MPInlineVideoFullscreenViewController";
static NSString * const VIDEO_CONTROLLER_CLASS_NAME_IOS8 = @"AVFullScreenViewController";

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    
    if ([[self.window.rootViewController presentedViewController] isKindOfClass:[MPMoviePlayerViewController class]])
    {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
    else
    {
        
        if ([[self.window.rootViewController presentedViewController] isKindOfClass:[UINavigationController class]])
        {
            
            // look for it inside UINavigationController
            UINavigationController *nc = (UINavigationController *)[self.window.rootViewController presentedViewController];
            
            // is at the top?
            if ([nc.topViewController isKindOfClass:[MPMoviePlayerViewController class]])
            {
                return UIInterfaceOrientationMaskAllButUpsideDown;
                
                // or it's presented from the top?
            }
            else if ([[nc.topViewController presentedViewController] isKindOfClass:[MPMoviePlayerViewController class]])
            {
                return UIInterfaceOrientationMaskAllButUpsideDown;
            }
        }
        else if ([[window.rootViewController presentedViewController] isKindOfClass:NSClassFromString(VIDEO_CONTROLLER_CLASS_NAME_IOS7)] ||
                 [[window.rootViewController presentedViewController] isKindOfClass:NSClassFromString(VIDEO_CONTROLLER_CLASS_NAME_IOS8)])
        {
            return UIInterfaceOrientationMaskAllButUpsideDown;
        }
    }
    
    return UIInterfaceOrientationMaskPortrait;
}

- (void)initGlobalFilters{
    
    G_Filters* g_Filters = [G_Filters getInstance];
    g_Filters.m_keyFilters = [[NSDictionary alloc] initWithObjectsAndKeys:@"n/a", [NSNumber numberWithInt:0],
                              @"C", [NSNumber numberWithInt:1],
                              @"C#/Db", [NSNumber numberWithInt:2],
                              @"D", [NSNumber numberWithInt:3],
                              @"D#/Eb", [NSNumber numberWithInt:4],
                              @"E", [NSNumber numberWithInt:5],
                              @"F", [NSNumber numberWithInt:6],
                              @"F#/Gb", [NSNumber numberWithInt:7],
                              @"G", [NSNumber numberWithInt:8],
                              @"G#/Ab", [NSNumber numberWithInt:9],
                              @"A", [NSNumber numberWithInt:10],
                              @"A#/Bb", [NSNumber numberWithInt:11],
                              @"B", [NSNumber numberWithInt:12],
                              nil];
    
   
    
    
    
    g_Filters.m_genreFilters = [[NSMutableDictionary alloc]init];
    
    
    NSString *urlAsString = [NSString stringWithFormat:@"http://www.organclubhouse.com/api/?option=get_category_list"];
    NSLog(@"URL = %@", urlAsString);
    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"GET"];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:nil error: nil];
    if(data == nil)
    {
        NSLog(@"Connection or Download failed.");
        return;
    }
    
    NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Results ==== %@", responseString);
    
    NSDictionary *dic1 = (NSDictionary*)[responseString JSONValue];
    

    for(int i =0 ;i<dic1.count;i++){
        
        NSString *name = [[dic1 valueForKey:@"NAME"] objectAtIndex:i];
        NSNumber *ID = [[dic1 valueForKey:@"ID"] objectAtIndex:i];
        [g_Filters.m_genreFilters  setObject:name forKey:ID];
    }

    g_Filters.m_instructorFilters = [[NSDictionary alloc] initWithObjectsAndKeys:@"Tj Hanes", [NSNumber numberWithInt:11],
                                     @"Carlton Whitfield", [NSNumber numberWithInt:0],
                                     @"Darrell Cook", [NSNumber numberWithInt:2],
                                     @"Shaun Martin", [NSNumber numberWithInt:3],
                                     @"Jermaine Roberts", [NSNumber numberWithInt:4],
                                     @"David Papanagis", [NSNumber numberWithInt:5],
                                     @"Peter Thompson", [NSNumber numberWithInt:7],
                                     @"Stephano Buchanan", [NSNumber numberWithInt:8],
                                     @"Anthony Brice", [NSNumber numberWithInt:9],
                                     @"Jermaine Roberts", [NSNumber numberWithInt:1],
                                     nil];
    
    
  /*
    g_Filters.m_instructorFilters = [[NSDictionary alloc] initWithObjectsAndKeys:@"Carlton Whitfield", [NSNumber numberWithInt:0],
                                @"Jermaine Roberts", [NSNumber numberWithInt:1],
                                @"Darrell Cook", [NSNumber numberWithInt:2],
                                @"Shaun Martin", [NSNumber numberWithInt:3],
                                @"Jermaine Roberts", [NSNumber numberWithInt:4],
                                nil];
    */
}


// If any update related to version or maintenance or No Network than this VC will be on Top when we open the app.
- (void)topViewController{

    UIStoryboard *mainStoryboard;
    if(IDIOM == IPAD){
        mainStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle: nil];
    }
    else{
        mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    }
    
    WarningViewController *yourController = (WarningViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"WarningViewController"];
    
    self.window.rootViewController = nil;
    self.window.rootViewController = yourController;

}






@end
