//
//  BrowseViewController.h
//  PianoClubHouse
//
//  Created by kingcode on 8/16/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoadingView.h"
#import "RRNCollapsableSectionTableViewController.h"
#import "BLMultiColorLoader.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
@import Firebase;
#import <SafariServices/SafariServices.h>

@class GADBannerView;

@interface BrowseViewController : RRNCollapsableTableViewController <NSURLConnectionDataDelegate,GADInterstitialDelegate,GADRewardBasedVideoAdDelegate,SFSafariViewControllerDelegate>
{
    BOOL isplayedfull;
    NSString *mp4_full;
    BOOL isactive;
}
@property (strong, nonatomic) IBOutlet GADBannerView *bannerView;
//@property (strong, nonatomic) IBOutlet GADBannerView *bannerview;

@property (strong, nonatomic) NSArray *menu;

@property (weak, nonatomic) IBOutlet UITableView *m_tableView;
@property (weak, nonatomic) IBOutlet UIView *m_navBar;
@property (weak, nonatomic) NSNumber *m_nCategory;
@property (strong, nonatomic) NSMutableArray *resultString;
@property (strong, nonatomic) NSDictionary *dicFilters;
@property (weak, nonatomic) IBOutlet UILabel *m_sortType;
@property (weak, nonatomic) IBOutlet UILabel *m_lbCategory;
@property (weak, nonatomic) IBOutlet UIImageView *m_imgTableBack;
@property (weak, nonatomic) IBOutlet BLMultiColorLoader *m_loader;

@property (weak, nonatomic) IBOutlet UIView *m_ProgressPopup;
@property (weak, nonatomic) IBOutlet UIProgressView *m_downProgressView;
@property (weak, nonatomic) IBOutlet UILabel *m_lbDownProgress;
@property (weak, nonatomic) IBOutlet UILabel *m_lbDownloadingTitle;

@property (strong, nonatomic) NSURLConnection *connectionManager;
@property (strong, nonatomic) NSMutableData *downloadedMutableData;
@property (strong, nonatomic) NSURLResponse *urlResponse;
@property (strong, nonatomic) IBOutlet UIImageView *Banner_topimageview;

@property (strong, nonatomic) IBOutlet UIView *Popup_view;

@property (strong, nonatomic) IBOutlet UIView *alpha_view;
@property (strong, nonatomic) IBOutlet UIButton *topImgAddSubscribeBtn;
@property(strong, nonatomic) NSString *catgoryName;

@property (strong, nonatomic) IBOutlet UIView *view_ratings;
@property (strong, nonatomic) IBOutlet UIView *view11;
@property (strong, nonatomic) IBOutlet UIView *view_rating_backView;

- (IBAction)onBackBtnClick:(id)sender;
- (IBAction)onSortBtnClick:(id)sender;
- (IBAction)onWatchOnlineBtnClick:(id)sender;
- (IBAction)onWatchOfflineBtnClick:(id)sender;

- (IBAction)onDownloadBtnClick:(UIButton *)sender;
- (IBAction)onDownloadCancel:(UIButton *)sender;

@end
