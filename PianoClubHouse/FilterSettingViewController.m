//
//  FilterSettingViewController.m
//  PianoClubHouse
//
//  Created by kingcode on 8/18/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "FilterSettingViewController.h"
#import "FilterCell.h"

@interface FilterSettingViewController ()

@end

@implementation FilterSettingViewController


@synthesize m_tbFilterPanel;
@synthesize m_dicFilterKeys;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [m_tbFilterPanel reloadData];
    
    NSLog(@"%@Filter === ", m_dicFilterKeys);
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Add this filter" style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return m_dicFilterKeys.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellIdentifier = @"filterCell";
    FilterCell* cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell)
    {
        cell = [[FilterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSDictionary* dic = [m_dicFilterKeys objectAtIndex:indexPath.row];
    
    cell.filterName.text = [dic objectForKey:@"Name"];
    //[cell.m_swSetFilter setOn:YES];
    return cell;
}


- (IBAction)onResetBtnClick:(id)sender {
    
    [m_tbFilterPanel reloadData];
}
@end
