//
//  VideoData.h
//  PianoClubHouse
//
//  Created by kingcode on 8/16/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoData : NSObject

@property (strong, nonatomic) NSString *video_id;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *artist;
@property (strong, nonatomic) NSString *publish_date;
@property (strong, nonatomic) NSString *video_length;
@property (strong, nonatomic) NSString *instructor;
@property (strong, nonatomic) NSString *song_key;
@property (strong, nonatomic) NSString *mp4_name;
@property (strong, nonatomic) NSString *previewID;

- (void) encodeWithCoder : (NSCoder *)encode;
- (id) initWithCoder : (NSCoder *)decode;

@end
