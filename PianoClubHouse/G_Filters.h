//
//  G_Filters.h
//  PianoClubHouse
//
//  Created by kingcode on 9/9/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface G_Filters : NSObject{

    NSDictionary *m_instructorFilters;
    NSDictionary *m_keyFilters;
    NSMutableDictionary *m_genreFilters;
}

@property(nonatomic,retain)NSDictionary *m_instructorFilters;
@property(nonatomic,retain)NSDictionary *m_keyFilters;
@property(nonatomic,retain)NSMutableDictionary *m_genreFilters;

+(G_Filters*)getInstance;

@end
