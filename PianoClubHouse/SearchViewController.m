//
//  SearchViewController.m
//  PianoClubHouse
//
//  Created by kingcode on 8/18/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "SearchViewController.h"
#import "FilterSettingViewController.h"
#import "InstructorFilterViewController.h"
#import "GenreFilterViewController.h"
#import "JSON.h"
#import "PostRequest.h"
#import "BrowseViewController.h"
#import "KeyFilterViewController.h"
#import "Common.h"
#import "G_Filters.h"
#import "HomeViewController.h"
#import "G_Functions.h"
#import "SearchResultViewController.h"

#define SEARCH_REQUEST_URL  @"http://organclubhouse.com/api/search.php"
#define NO_FILTERS  @"no filters added yet..."
#define kOFFSET_FOR_KEYBOARD 120.0

@interface SearchViewController ()
{
    LoadingView* loadingView;
    NSTimer* mainTimer;
    NSMutableArray* result;
}

@end

@implementation SearchViewController

@synthesize m_dicFilterKeys;
@synthesize m_navbar;
@synthesize m_instructorFilters;
@synthesize m_genreFilters;
@synthesize m_keyFilters;
@synthesize m_keywords;
@synthesize m_txtKeyword;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if([G_Functions isPremiumMode])
      //  self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"search_back"]];

    if(IDIOM == IPAD)
        m_navbar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar_ipad"]];
    else
        m_navbar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"browse_title"]];
    
    
    
    m_txtKeyword.layer.cornerRadius = 5;
    m_txtKeyword.layer.borderColor = [[UIColor colorWithRed:37/255.f green:14/255.f blue:50/255.f alpha:1] CGColor];
    m_txtKeyword.layer.borderWidth = 1;
    
    
    m_instructorFilters = [[NSMutableArray alloc] init];
    m_genreFilters = [[NSMutableArray alloc] init];
    m_keyFilters = [[NSMutableArray alloc] init];
    m_keywords = [[NSMutableArray alloc] init];
    m_txtKeyword.delegate = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    [self.view sendSubviewToBack:self.m_scrollView];
}

-(void)dismissKeyboard {
    [m_txtKeyword resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"instructorFilterSegue"]) {
        
        InstructorFilterViewController *destViewController = segue.destinationViewController;
        destViewController.m_arrSelected = [[NSMutableArray alloc] initWithArray:m_instructorFilters];
    }
    else if ([segue.identifier isEqualToString:@"genreFilterSegue"]) {
        
        GenreFilterViewController *destViewController = segue.destinationViewController;
        destViewController.m_arrSelected = [[NSMutableArray alloc] initWithArray:m_genreFilters];
    }
    else if ([segue.identifier isEqualToString:@"keyFilterSegue"]) {
        
        KeyFilterViewController *destViewController = segue.destinationViewController;
        destViewController.m_selectedKeys = [[NSMutableArray alloc] initWithArray:m_keyFilters];
    }
    else if ([segue.identifier isEqualToString:@"displaySegue"]){
        NSLog(@"%@",m_instructorFilters);
        
        NSMutableArray *ar1update = [m_instructorFilters mutableCopy];
        if ([ar1update count] == 1)
        {
            
            NSString *str =  [NSString stringWithFormat:@"%@", [ar1update objectAtIndex:0]];

           if ( [str  isEqual: @"1"])
           {
               ar1update[0] = @"4";
           }
            else
            {
                ar1update[0] = @"1";
            }
        }
        NSLog(@"%@",ar1update);
        NSDictionary *dicFilters = @{@"instructor":ar1update, @"key":m_keyFilters, @"genre":m_genreFilters, @"keyword":m_txtKeyword.text};
        SearchResultViewController *destViewController = segue.destinationViewController;
        destViewController.dicFilters = [[NSDictionary alloc] initWithDictionary:dicFilters];
    }
    
    [self.view endEditing:YES];
}


- (IBAction)onBackBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onDisplayBtnClick:(id)sender {
    
    if([m_instructorFilters count] == 0 && [m_keyFilters count] == 0 && [m_genreFilters count] == 0 && [m_txtKeyword.text length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OrganClubHouse" message:@"No Filter Selected" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    [self performSegueWithIdentifier:@"displaySegue" sender:self];
    //NSDictionary *dicFilters = @{@"instructor":m_instructorFilters, @"key":m_keyFilters, @"genre":m_genreFilters, @"keyword":m_txtKeyword.text};

    //HomeViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"home_view"];
    //[self.navigationController popToViewController:vc animated:YES];
    
//    NSInteger i = 20;
//    destViewController.m_nCategory  = [NSNumber numberWithInteger:i];
//    destViewController.dicFilters = [[NSDictionary alloc] initWithDictionary:dicFilters];
//    [self.navigationController pushViewController:destViewController animated:YES];
}

- (IBAction)onResetAllBtnClick:(id)sender {
    
    [m_instructorFilters removeAllObjects];
    [m_keyFilters removeAllObjects];
    [m_genreFilters removeAllObjects];
    [appdelegate.m_arrSelectedInstructors removeAllObjects];
    m_txtKeyword.text = @"";
    
    [self displaySelectedFilters];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)keyboardWillShow {
    // Animate the current view out of the way
    if (self.m_scrollView.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.m_scrollView.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide {
    if (self.m_scrollView.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.m_scrollView.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.m_scrollView.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.m_scrollView.frame = rect;
    
    [UIView commitAnimations];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    //move the main view, so that the keyboard does not hide it.
//    if  (self.view.frame.origin.y >= 0)
//    {
//        [self setViewMovedUp:YES];
//    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [self displaySelectedFilters];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)displaySelectedFilters
{
    G_Filters *gFilters = [G_Filters getInstance];
    //Key Filters
    if([m_keyFilters count] > 0){
        
        NSString *s_Key = [[NSString alloc] init];
        for(NSNumber *iterator in m_keyFilters)
        {
            NSString *temp = [gFilters.m_keyFilters objectForKey:iterator];
            s_Key = [s_Key stringByAppendingString:[NSString stringWithFormat:@"%@, ", temp]];
        }
        s_Key = [self removeLastCommaFromString:s_Key];
        self.m_lbKeyFilters.text = s_Key;
        self.m_lbKeyFilters.textColor = Rgb2UIColor(37, 14, 50);
    }
    else{
        
        self.m_lbKeyFilters.text = NO_FILTERS;
        self.m_lbKeyFilters.textColor = Rgb2UIColor(102, 102, 102);
    }
    
    //Genre Filters
    if([m_genreFilters count] > 0){
        
        NSString *s_Key = [[NSString alloc] init];
         NSLog(@"%@",gFilters.m_genreFilters);
        for(NSNumber *iterator in m_genreFilters)
        {
           
            NSLog(@"%@",iterator);
            NSString *temp = [gFilters.m_genreFilters objectForKey:iterator];
            
            NSLog(@"%@",temp);
            
            s_Key = [s_Key stringByAppendingString:[NSString stringWithFormat:@"%@, ", temp]];
        }
        s_Key = [self removeLastCommaFromString:s_Key];
        self.m_lbGenreFilters.text = s_Key;
        self.m_lbGenreFilters.textColor = Rgb2UIColor(37, 14, 50);
    }
    else{
        
        self.m_lbGenreFilters.text = NO_FILTERS;
        self.m_lbGenreFilters.textColor = Rgb2UIColor(102, 102, 102);
    }
    
    //Instructor Filters
    if([m_instructorFilters count] > 0){
        
        NSString *s_Key = [[NSString alloc] init];
        for(NSNumber *iterator in m_instructorFilters)
        {
            NSString *temp = [gFilters.m_instructorFilters objectForKey:iterator];
            s_Key = [s_Key stringByAppendingString:[NSString stringWithFormat:@"%@, ", temp]];
        }
        s_Key = [self removeLastCommaFromString:s_Key];
        self.m_lbInstructorFilters.text = s_Key;
        self.m_lbInstructorFilters.textColor = Rgb2UIColor(37, 14, 50);
    }
    else{
        
        self.m_lbInstructorFilters.text = NO_FILTERS;
        self.m_lbInstructorFilters.textColor = Rgb2UIColor(102, 102, 102);
    }
}

- (NSString*)removeLastCommaFromString:(NSString*)str
{
    NSRange lastComma = [str rangeOfString:@"," options:NSBackwardsSearch];
    
    if(lastComma.location != NSNotFound) {
        str = [str stringByReplacingCharactersInRange:lastComma
                                           withString: @" "];
    }
    return str;
}

@end
