//
//  BrowseViewController.m
//  PianoClubHouse
//
//  Created by kingcode on 8/16/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "BrowseViewController.h"
#import "ListCell.h"
#import "LoadingView.h"
#import "JSON.h"
#import "SearchViewController.h"
#import "PreviewViewController.h"
#import "G_Functions.h"
#import "KxMenu.h"
#import "PostRequest.h"
#import "FakeModelBuilder.h"
#import "G_Filters.h"
#import "MyList.h"
#import "VideoData.h"
#import "UIButton+Tagged.h"
#import "Common.h"
#import "CheckFileUrl.h"
@import GoogleMobileAds;
@import MediaPlayer;
@import Firebase;

#define SERVER_URL_FOR_GUEST  @"http://organclubhouse.com/api/?option=get_video&category=0&standardview=1"
#define SERVER_URL  @"http://organclubhouse.com/api?option=get_video&category=%@"
#define GET_FILTERKEY_URL  @"http://organclubhouse.com/api?option=get_filterkeys"
#define SEARCH_REQUEST_URL  @"http://organclubhouse.com/api/search.php"


@interface BrowseViewController ()
{
    BOOL videoWatched;
    NSArray* _videoData;
    LoadingView* loadingView;
    NSTimer* mainTimer;
    
    NSDictionary *m_dicFilterKeys;
    NSDictionary* m_dicInstructors;
    NSDictionary* m_dicKeys;
    NSDictionary* m_dicGenres;
    
    BOOL m_bTitleAscending;
    BOOL m_bArtistAscending;
    BOOL m_bDateAscending;
    
    NSInteger m_nCount;
    MPMoviePlayerViewController *videoPlayerView;
    NSURL *m_currentVideoURL;
    CheckFileUrl* checkObj;
    
    int nDownloadingCount;
    BOOL bAllowRotate;
    
    NSString* sDownloadFileName;
    NSInteger nDownloadIndex;
    int rate;
    
}
/// The interstitial ad.
@property(nonatomic, strong) GADInterstitial *interstitial;
@end

@implementation BrowseViewController

@synthesize m_nCategory;
@synthesize m_tableView;
@synthesize m_navBar;
@synthesize resultString;
@synthesize dicFilters;
@synthesize m_sortType;
@synthesize m_loader;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    rate = [[NSUserDefaults standardUserDefaults]integerForKey:@"rateUs"];

    
    if (rate == 5)
    {
        _view_ratings.layer.cornerRadius = 10;
        
        _view11.hidden = false;
        _view_rating_backView.hidden = false;
    
      //  UITabBarController.tabBar.userInteractionEnabled = NO;
    }
    else
    {
        _view11.hidden = true;
        _view_rating_backView.hidden = true;
    }
    
    
    _Popup_view.layer.cornerRadius = 5;
    _Popup_view.layer.masksToBounds = YES;
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:_Popup_view.bounds];
    _Popup_view.layer.masksToBounds = NO;
    _Popup_view.layer.shadowColor = [UIColor blackColor].CGColor;
    _Popup_view.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    _Popup_view.layer.shadowOpacity = 0.5f;
    _Popup_view.layer.shadowPath = shadowPath.CGPath;
    
    [self.alpha_view setHidden:true];

    
    if(![G_Functions isPremiumMode])
    {
        // self.m_imgTableBack.image = [UIImage imageNamed:@"table_back_pub"];
        
    }
    
    if([G_Functions IsGuestMode]){
        
       // self.m_imgTableBack.image = [UIImage imageNamed:@"table_back.png"];
        
        [self.m_imgTableBack setImage:[UIImage imageNamed:@"table_back_white"]];
        [self.bannerView setHidden:false];
        [self.Banner_topimageview setHidden:false];
        [self.topImgAddSubscribeBtn setHidden:false];
        
        NSURL *imageURL = [NSURL URLWithString:@"http://www.organclubhouse.com/images/bannermobile/active.jpg"];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        // Update the UI
        self.Banner_topimageview.image = [UIImage imageWithData:imageData];
         
        isplayedfull = false;
        isactive = false;
        [self createAndLoadInterstitial];
        [self requestRewardedVideo];
        [GADRewardBasedVideoAd sharedInstance].delegate = self;
        // Replace this ad unit ID with your own ad unit ID.
        
        self.bannerView.adUnitID = @"ca-app-pub-6153266407346561/2993511938";
        self.bannerView.rootViewController = self;
        
        
        GADRequest *request = [GADRequest request];
        // Requests test ads on devices you specify. Your test device ID is printed to the console when
        // an ad request is made. GADBannerView automatically returns test ads when running on a
        // simulator.
        
        request.testDevices = @[
                                @"2077ef9a63d2b398840261c8221a0c9a"  // Eric's iPod Touch
                                ];
       
        [self.bannerView loadRequest:request];
      
        self.m_tableView.frame =CGRectMake(0, self.m_tableView.frame.origin.y, CGRectGetWidth(m_tableView.frame), CGRectGetHeight(self.view.frame)-self.m_tableView.frame.origin.y-self.bannerView.frame.size.height);
        
    }
    
    else{
        
        [self.m_imgTableBack setImage:[UIImage imageNamed:@"table_back"]];
        self.m_navBar.frame =  CGRectMake(0, 20, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.m_navBar.frame));
        
        
        int xxx = 0;
        if(IDIOM == IPAD){
            xxx = 35;
        }
        else{
            xxx=10;
        }
        
         self.m_tableView.frame = CGRectMake(0, CGRectGetMaxY(self.m_navBar.frame),CGRectGetWidth(self.m_tableView.frame), CGRectGetHeight(self.m_tableView.frame)+ self.bannerView.frame.size.height + self.m_navBar.frame.size.height + xxx);
        
        
        [self.bannerView setHidden:true];
        [self.Banner_topimageview setHidden:true];
        [self.topImgAddSubscribeBtn setHidden:true];
       
        
    }
    
    
    [self GetDataFromServer];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]];
    m_navBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"browse_title"]];
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        m_navBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar_ipad"]];
    
    NSLog(@"Category ==== %@", m_nCategory);
    
    m_bTitleAscending = NO;
    m_bArtistAscending = NO;
    m_bDateAscending = NO;
    
    nDownloadingCount = 0;
    
    m_loader.lineWidth = 4.0;
    m_loader.colorArray = [NSArray arrayWithObjects:[UIColor redColor],
                           [UIColor purpleColor],
                           [UIColor greenColor],
                           [UIColor blueColor],
                           [UIColor whiteColor],nil];
    
    
    [self showCategoryLabel];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadNotificationReceive:) name:@"DownloadingStartNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadNotificationReceive:) name:@"DownloadingEndNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceOrientationPortrait) name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];
    
    
    
    
    
    
    
    
    
}

- (void)createAndLoadInterstitial {
    
    self.interstitial =
    [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-6153266407346561/7423711531"];
    GADRequest *request = [GADRequest request];
    // Request test ads on devices you specify. Your test device ID is printed to the console when
    // an ad request is made.
    //self.interstitial.delegate = self;
    //request.testDevices = @[ kGADSimulatorID, @"2077ef9a63d2b398840261c8221a0c9a" ];
    [self.interstitial loadRequest:request];
    _interstitial.delegate =self;
    
    if (self.interstitial.isReady) {
        [self.interstitial presentFromRootViewController:self];
    }
    else
    {
        [self performSelector:@selector(setHidden) withObject:self afterDelay:1.800];
        
    }
    // [_interstitial presentFromRootViewController:self];
    
}

- (BOOL)shouldAutorotate
{
    return bAllowRotate;
}


- (void)forceOrientationPortrait
{
    bAllowRotate = YES;
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInt:UIInterfaceOrientationPortrait] forKey:@"orientation"];
    bAllowRotate = NO;
}


-(void)viewWillAppear:(BOOL)animated
{
    
    
    [super viewWillAppear:animated];
    
    
    [m_tableView reloadData];
}

-(void)setHidden
{
    if (self.interstitial.isReady) {
        [self.interstitial presentFromRootViewController:self];
    }
    else
    {
        [self performSelector:@selector(setHidden) withObject:self afterDelay:1.800];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(NSArray *)menu:(NSMutableArray*)array  {
    
    _menu = [FakeModelBuilder buildHomeListMenu:array];
    return _menu;
}

-(void)downloadNotificationReceive:(NSNotification *) notification
{
    if([[notification name] isEqualToString:@"DownloadingStartNotification"])
    {
        if(nDownloadingCount == 0)
        {
            m_navBar.userInteractionEnabled = NO;
            self.tabBarController.tabBar.userInteractionEnabled = NO;
        }
        
        nDownloadingCount++;
        NSLog (@"Successfully received DownloadingStartNotification!");
    }
    else if([[notification name] isEqualToString:@"DownloadingEndNotification"])
    {
        nDownloadingCount--;
        if(nDownloadingCount == 0)
        {
            m_navBar.userInteractionEnabled = YES;
            self.tabBarController.tabBar.userInteractionEnabled = YES;
        }
        NSLog (@"Successfully received DownloadingEndNotification!");
    }
}

#pragma mark - RRNCollapsableTableView

-(NSString *)sectionHeaderNibName
{
    if([G_Functions isPremiumMode])
    {
        return IDIOM == IPAD ? @"MenuSectionHeaderView_iPad" : @"MenuSectionHeaderView";
    }
    else if([G_Functions IsGuestMode]){
        return IDIOM == IPAD ? @"MenuSectionHeaderView_iPad1" : @"MenuSectionHeaderView1";
    }
    else{
        return @"MenuSectionHeaderView1";
        
    }
}

-(NSArray *)model {
    return self.menu;
}

-(UITableView *)collapsableTableView {
    return m_tableView;
}

- (void)showCategoryLabel{
    
    NSString* temp = nil;
    if(m_nCategory == nil)
    {
        if([G_Functions IsGuestMode]){
            temp = @"Free Tutorials";
        }else{
            temp = @"Search Results";
            
        }
    }
    else
    {
        temp = self.catgoryName;
    }
    
    self.m_lbCategory.text = temp;
}

#pragma marks - Sort, Search

- (void)onBtnSearchClicked
{
    [self performSegueWithIdentifier:@"searchSegue" sender:self];
}

-(void)sortTableDataUsingSortKey:(NSString *)sortKey withAscending:(BOOL)isAscending
{
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:sortKey ascending:isAscending]];
    NSArray *arrTemp = [[NSArray alloc] initWithArray:[resultString sortedArrayUsingDescriptors:sortDescriptors]];
    [resultString count]?[resultString removeAllObjects]:NSLog(@"Datsource Table Not null");
    [resultString addObjectsFromArray:arrTemp];
    arrTemp = nil;
    
    [self menu:resultString];
    [m_tableView reloadData];
}

- (void)GetDataFromServer
{
    loadingView = nil;
    loadingView = [LoadingView loadingViewInView:self.view withTitle:NSLocalizedString(@"Loading...", nil)];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    mainTimer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(onStartTimer) userInfo:nil repeats:YES];
}


-(void) onStartTimer
{
    if(dicFilters == nil)
        [self LoadVideoList];
    else
        [self getSearchResult];
}

-(void)loadFilterKeysFromServer
{
    NSString *urlAsString = [NSString stringWithFormat:@"%@", GET_FILTERKEY_URL];
    NSLog(@"URL = %@", urlAsString);
    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"GET"];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:nil error: nil];
    if(data == nil)
    {
        NSLog(@"Connection or Download failed.");
        return;
    }
    NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Results ==== %@", responseString);
    
    m_dicFilterKeys = (NSDictionary*)[responseString JSONValue];
}

-(void) LoadVideoList
{
    
    [mainTimer invalidate];
    mainTimer = nil;
    
    NSString* strCategory = [m_nCategory stringValue];
    NSString *urlAsString;
    
    if([G_Functions IsGuestMode]){
        urlAsString = [NSString stringWithFormat:SERVER_URL_FOR_GUEST];
    }
    else{
        urlAsString = [NSString stringWithFormat:SERVER_URL, strCategory];
    }
    NSLog(@"URL = %@", urlAsString);
    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:@"GET"];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:nil error: nil];
    if(data == nil)
    {
        NSLog(@"Connection or Download failed.");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OrganClubHouse" message:@"Connection or Download failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [loadingView removeView];
        loadingView = nil;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        return;
    }
    NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if([responseString isEqualToString:@"[]"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OrganClubHouse" message:@"No videos found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [loadingView removeView];
        loadingView = nil;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        return;
    }
    NSLog(@"Results ==== %@", responseString);
    
    
    resultString = (NSMutableArray*)[responseString JSONValue];
    
    m_nCount = [resultString count];
    self.m_sortType.text = [NSString stringWithFormat:@"%lu videos, sorted by publish date", (unsigned long)m_nCount];
    //m_videoNum.text = [NSString stringWithFormat:@"%lu videos", (unsigned long)[resultString count]];
    
    [self loadFilterKeysFromServer];        // Load Filter Keys
    
    
    [loadingView removeView];
    loadingView = nil;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    [self menu:resultString];
    [m_tableView reloadData];
    
}

-(void)getSearchResult
{
    [mainTimer invalidate];
    mainTimer = nil;
    
    NSString *jsonString;
    
    if ([NSJSONSerialization isValidJSONObject:dicFilters])
    {
        // Serialize the dictionary
        NSData* json = [NSJSONSerialization dataWithJSONObject:dicFilters options:NSJSONWritingPrettyPrinted error:nil];
        
        // If no errors, let's view the JSON
        if (json != nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"JSON: %@", jsonString);
            
            NSString* responseString = [PostRequest postDataToUrl:SEARCH_REQUEST_URL jsonString:jsonString];
            
            if(responseString == nil)
            {
                NSLog(@"Connection or Download failed.");
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OrganClubHouse" message:@"Connection or Download failed" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
                [loadingView removeView];
                loadingView = nil;
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                
                return;
            }
            
            if([responseString isEqualToString:@"[]"])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OrganClubHouse" message:@"No videos found" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
                [loadingView removeView];
                loadingView = nil;
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                
                return;
            }
            
            resultString = (NSMutableArray*)[responseString JSONValue];
            
            [self menu:resultString];
            [m_tableView reloadData];
            m_nCount = [resultString count];
            self.m_sortType.text = [NSString stringWithFormat:@"%lu videos, sorted by publish date", (unsigned long)m_nCount];
            //m_videoNum.text = [NSString stringWithFormat:@"%lu videos", (unsigned long)[resultString count]];
            
        }
    }
    
    [loadingView removeView];
    loadingView = nil;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}


#pragma mark - UITableView

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return IDIOM == IPAD? 86.0f : 45.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self model].count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellIdentifier = @"listCell";
    ListCell* cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(!cell)
    {
        cell = [[ListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"detail_bg"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0]];
    
    NSDictionary* dic = [resultString objectAtIndex:indexPath.section];
    NSString* videoTitle = [dic objectForKey:@"VideoTitle"]; videoTitle =  !videoTitle? @"": videoTitle;
    NSString* videoArtist = [dic objectForKey:@"VideoArtist"]; //videoArtist =  !videoArtist? @"": videoArtist;
    NSString* videoID = [dic objectForKey:@"VideoID"];
    NSString* publishDate = [dic objectForKey:@"PublishDate"];  publishDate =  !publishDate? @"": publishDate;
    NSString* runtime = [dic objectForKey:@"VideoRuntime"];     runtime =  !runtime? @"": runtime;
    NSString* instructor = [dic objectForKey:@"InstructorID"];    instructor =  !instructor? @"": instructor;
    NSString* songkey = [dic objectForKey:@"SongKey"];    songkey =  !songkey? @"": songkey;
    NSString* mp4Name = [dic objectForKey:@"MP4Name"];    mp4Name =  !mp4Name? @"": mp4Name;
    NSString* previewID = [G_Functions getSafeString:[dic objectForKey:@"YouTubePreviewID"]];
    publishDate = [self convertDateFormat:publishDate];
    
    cell.video_id = videoID;        [cell refresh];
    cell.title = videoTitle;
    cell.artist = videoArtist;
    cell.preview_id = previewID;
    cell.m_lbVideoLength.text = [NSString stringWithFormat:@"%@ min", runtime];
    cell.m_lbPublishDate.text = publishDate;
    cell.m_lbSongKey.text = [self getSongKey:songkey];
    cell.m_lbInstructor.text = [self getInstructor:instructor];
    cell.mp4_name = mp4Name;
    
    cell.m_btnPreview.stringTag = [NSString stringWithFormat:@"%@:%@", previewID, videoTitle];//previewID;
    cell.m_btnWatch.stringTag = mp4Name;
    cell.m_btnWatchOffline.stringTag = mp4Name;
    cell.m_btnDownload.tag = indexPath.section;
    cell.m_lbl_Artist.text = videoArtist;

    if([G_Functions IsGuestMode]){
        
        
        [(UILabel*)[cell viewWithTag:101] setTextColor:[UIColor whiteColor]];
        [(UILabel*)[cell viewWithTag:102] setTextColor:[UIColor whiteColor]];
        [(UILabel*)[cell viewWithTag:103] setTextColor:[UIColor whiteColor]];
        [(UILabel*)[cell viewWithTag:104] setTextColor:[UIColor whiteColor]];
        [(UILabel*)[cell viewWithTag:105] setTextColor:[UIColor whiteColor]];
        cell.contentView.backgroundColor = Rgb2UIColor(37, 14, 50);
        cell.m_lbSongKey.textColor = [UIColor whiteColor];
        cell.m_lbVideoLength.textColor = [UIColor whiteColor];
        cell.m_lbPublishDate.textColor = [UIColor whiteColor];
        cell.m_lbInstructor.textColor = [UIColor whiteColor];
        cell.m_lbl_Artist.textColor = [UIColor whiteColor];
    }
    
    
    
    return cell;
}

-(VideoData*)getVideoDataByID:(NSString*)video_id
{
    for(NSDictionary* dic in resultString)
    {
        if([video_id isEqualToString:[dic objectForKey:@"VideoID"]])
        {
            VideoData* pData = [[VideoData alloc] init];
            
            pData.video_id = video_id;
            pData.title = [dic objectForKey:@"VideoTitle"];
            pData.artist = [dic objectForKey:@"VideoArtist"];
            pData.previewID = [dic objectForKey:@"YouTubePreviewID"];
            pData.video_length = [dic objectForKey:@"VideoRuntime"];
            pData.publish_date = [dic objectForKey:@"PublishDate"];
            pData.instructor = [self getInstructor:[dic objectForKey:@"InstructorID"]];
            pData.song_key = [self getSongKey:[dic objectForKey:@"SongKey"]];
            pData.mp4_name = [dic objectForKey:@"MP4Name"];
            
            return pData;
        }
    }
    
    return nil;
}

-(NSString*)getSongKey:(NSString*)keyID
{
    
    
    //    if (self.interstitial.isReady) {
    //        [self.interstitial presentFromRootViewController:self];
    //    }
    //    else
    //    {
    //
    //    }
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *myNumber = [f numberFromString:keyID];
    
    G_Filters *gFilters = [G_Filters getInstance];
    NSString *songKey = [gFilters.m_keyFilters objectForKey:myNumber];
    return songKey;
}

-(NSString*)getInstructor:(NSString*)instructorID
{
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *myNumber = [f numberFromString:instructorID];
    
    int value = [myNumber intValue];
    myNumber = [NSNumber numberWithInt:value - 1];
    
    G_Filters *gFilters = [G_Filters getInstance];
    NSString *instructor = [gFilters.m_instructorFilters objectForKey:myNumber];
    return instructor;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    if ([m_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [m_tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([m_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [m_tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}


#pragma marks - DateFormatter

-(NSString*)convertDateFormat:(NSString*)str
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *date = [dateFormatter dateFromString: str];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yy"];
    
    NSString *convertedString = [dateFormatter stringFromDate:date];
    NSLog(@"Converted String : %@",convertedString);
    
    return convertedString;
}

#pragma marks - Go to Detail Page

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"searchSegue"]){
        SearchViewController *destViewController = segue.destinationViewController;
        destViewController.m_dicFilterKeys = m_dicFilterKeys;
    }
    else if ([segue.identifier isEqualToString:@"previewSegue"]) {
        PreviewViewController *destViewController = segue.destinationViewController;
        NSString* string = ((UIButton*)sender).stringTag;
        
        NSArray* list = [string componentsSeparatedByString:@":"];
        NSString* preview_id = [list objectAtIndex:0];
        NSString* sTitle = [list objectAtIndex:1];
        
        destViewController.m_sPreviewID = preview_id;
        destViewController.m_sTitle = sTitle;
    }
    
}

- (IBAction)onBackBtnClick:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#define DISABLE_VALUE  0.7f
#define ENABLE_VALUE   1.0f

-(void)disableBackground:(BOOL)disable
{
    if(disable)
    {
        self.m_navBar.userInteractionEnabled = NO;
        self.m_tableView.userInteractionEnabled = NO;
        self.tabBarController.tabBar.userInteractionEnabled = NO;
        
        self.m_tableView.alpha = DISABLE_VALUE;
        self.m_navBar.alpha = DISABLE_VALUE;
    }
    else
    {
        self.m_navBar.userInteractionEnabled = YES;
        self.m_tableView.userInteractionEnabled = YES;
        self.tabBarController.tabBar.userInteractionEnabled = YES;
        
        self.m_tableView.alpha = ENABLE_VALUE;
        self.m_navBar.alpha = ENABLE_VALUE;
    }
}

//-(void)disableBackground:(BOOL)disable
//{
//    if(disable)
//    {
//        self.view.backgroundColor = [UIColor blackColor];
//        self.view.alpha = 0.8f;
//        self.view.userInteractionEnabled = NO;
//        self.tabBarController.tabBar.userInteractionEnabled = NO;
//    }
//    else
//    {
//        self.view.backgroundColor = [UIColor whiteColor];
//        self.view.alpha = 1.0f;
//        self.view.userInteractionEnabled = YES;
//        self.tabBarController.tabBar.userInteractionEnabled = YES;
//    }
//}

- (IBAction)onWatchOnlineBtnClick:(id)sender {
    
    
    if(![G_Functions IsGuestMode]){
        
        videoWatched = false;
        NSString* mp4Name = ((UIButton*)sender).stringTag;
        m_currentVideoURL = [NSURL URLWithString:[NSString stringWithFormat:DOWNLOAD_URL, mp4Name]];
        
        NSLog(@"vurl %@",m_currentVideoURL);
        
        [m_loader startAnimation];
        [self disableBackground:YES];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startPlay) name:@"ValidFileNotification" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(alertVideo) name:@"InValidFileNotification" object:nil];
        
        if(checkObj == nil){
            checkObj = [[CheckFileUrl alloc] init];
        }
        

        [checkObj checkUrl:m_currentVideoURL];

        
        
    }
    
    else{
    
        if (![[GADRewardBasedVideoAd sharedInstance] isReady]) {
            [self requestRewardedVideo];
        }
    
        NSInteger valueToSave = [[NSUserDefaults standardUserDefaults] integerForKey:@"VideoToWatch"];
   
        if (valueToSave < 5 || videoWatched){
            videoWatched = false;
            valueToSave ++;
            [[NSUserDefaults standardUserDefaults] setInteger:valueToSave forKey:@"VideoToWatch" ];
            [[NSUserDefaults standardUserDefaults] synchronize];
        
            NSString* mp4Name = ((UIButton*)sender).stringTag;
            m_currentVideoURL = [NSURL URLWithString:[NSString stringWithFormat:DOWNLOAD_URL, mp4Name]];
        
            NSLog(@"vurl %@",m_currentVideoURL);
        
            [m_loader startAnimation];
            [self disableBackground:YES];
        
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startPlay) name:@"ValidFileNotification" object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(alertVideo) name:@"InValidFileNotification" object:nil];
            
            if(checkObj == nil)
                checkObj = [[CheckFileUrl alloc] init];

            [checkObj checkUrl:m_currentVideoURL];

        
        }
        else{
            if (isactive == false)
            {
                isactive = true;
                mp4_full = ((UIButton*)sender).stringTag;
                double delayInSeconds = 2.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    if ([[GADRewardBasedVideoAd sharedInstance] isReady]) {
                        isactive = false;
                        [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:self];
                    } else {
                        isactive = false;
                        [[[UIAlertView alloc]
                          initWithTitle:@""
                          message:@"Some error occured. Try after some time."
                          delegate:self
                          cancelButtonTitle:@"ok"
                          otherButtonTitles:nil] show];
                    }
                });
            
            }
            else
            {
            
            }
        
        
        
        }
    }
}

- (void)requestRewardedVideo {
    GADRequest *request = [GADRequest request];
    [[GADRewardBasedVideoAd sharedInstance] loadRequest:request
                                           withAdUnitID:@"ca-app-pub-6153266407346561/1377177938"];
}

- (void)startPlay
{
    if (videoPlayerView)
    {
        videoPlayerView = nil;
    }
    videoPlayerView = [[MPMoviePlayerViewController alloc] initWithContentURL:m_currentVideoURL];
    [self presentMoviePlayerViewControllerAnimated:videoPlayerView];
    [videoPlayerView.moviePlayer play];
    NSLog(@"starting");
    [m_loader stopAnimation];
    [self disableBackground:NO];
    
    NSLog(@"Removed 1");
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ValidFileNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"InValidFileNotification" object:nil];
}

- (void)alertVideo
{
    [m_loader stopAnimation];
    [self disableBackground:NO];
    
    NSLog(@"Removed 2");

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ValidFileNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"InValidFileNotification" object:nil];
    
    UIAlertView *confirmDlg=[[UIAlertView alloc]initWithTitle:@"OrganClubhouse" message:@"This file does not exist." delegate:self
                                            cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [confirmDlg show];
}

- (IBAction)onWatchOfflineBtnClick:(id)sender
{
    NSString* mp4Name  = ((UIButton*)sender).stringTag;
    NSURL *videoURL;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString* filePath = [documentsDirectory stringByAppendingPathComponent:mp4Name];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if(!fileExists)
    {
        NSLog(@"=====No file====");
        return;
    }
    
    videoURL = [NSURL fileURLWithPath:filePath];
    NSLog(@"vurl %@",videoURL);
    videoPlayerView = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
    [self presentMoviePlayerViewControllerAnimated:videoPlayerView];
    [videoPlayerView.moviePlayer play];
}

#pragma marks - Sort

- (IBAction)onSortBtnClick:(id)sender {
    [self showMenu:sender];
}

- (void)showMenu:(UIButton *)sender
{
    NSArray *menuItems =
    @[
      
      [KxMenuItem menuItem:@"Sort By Title"
                     image:nil
                    target:self
                    action:@selector(sortByTitle)],
      
      [KxMenuItem menuItem:@"Sort By Artist"
                     image:nil
                    target:self
                    action:@selector(sortByArtist)],
      
      [KxMenuItem menuItem:@"Sort By Date"
                     image:nil
                    target:self
                    action:@selector(sortByDate)],
      ];
    
    [KxMenu showMenuInView:self.view
                  fromRect:sender.frame
                 menuItems:menuItems];
}

-(void)sortByTitle
{
    m_bTitleAscending = !m_bTitleAscending;
    self.m_sortType.text = [NSString stringWithFormat:@"%lu videos, sorted by title", (unsigned long)m_nCount];
    [self sortTableDataUsingSortKey:@"VideoTitle" withAscending:m_bTitleAscending];
}

-(void)sortByArtist
{
    m_bArtistAscending = !m_bArtistAscending;
    self.m_sortType.text = [NSString stringWithFormat:@"%lu videos, sorted by artist", (unsigned long)m_nCount];
    [self sortTableDataUsingSortKey:@"VideoArtist" withAscending:m_bArtistAscending];
}

-(void)sortByDate
{
    m_bDateAscending = !m_bDateAscending;
    self.m_sortType.text = [NSString stringWithFormat:@"%lu videos, sorted by publish date", (unsigned long)m_nCount];
    [self sortTableDataUsingSortKey:@"PublishDate" withAscending:m_bDateAscending];
}

#pragma mark - Download

- (IBAction)onDownloadBtnClick:(UIButton *)sender {
    
    self.downloadedMutableData = [[NSMutableData alloc] init];
    nDownloadIndex = sender.tag;
    sDownloadFileName = [[resultString objectAtIndex:nDownloadIndex] objectForKey:@"MP4Name"];
    self.m_lbDownloadingTitle.text = [[resultString objectAtIndex:nDownloadIndex] objectForKey:@"VideoTitle"];
    
    if(sDownloadFileName == nil)
    {
        NSLog(@"=== mp4 name is nil ===");
        UIAlertView *confirmDlg=[[UIAlertView alloc]initWithTitle:APP_LOGO message:@"This file does not exist." delegate:self
                                                cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [confirmDlg show];
        return;
    }
    
    self.m_ProgressPopup.hidden = NO;
    [self disableBackground:YES];
    
    self.m_downProgressView.progress = 0.0f;
    self.m_lbDownProgress.text = @"0%";
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:DOWNLOAD_URL, sDownloadFileName]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                            timeoutInterval:60.0];
    
    self.connectionManager = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"%lld", response.expectedContentLength);
    if(response.expectedContentLength == -1){
        [self cancelDownload];
        NSString* msgBody = @"Error downloading video. Please contact administrator.";
        UIAlertView *errorAlert=[[UIAlertView alloc]initWithTitle:APP_LOGO message:msgBody delegate:self
                                                cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [errorAlert show];
    }
    self.urlResponse = response;
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.downloadedMutableData appendData:data];
    self.m_downProgressView.progress = ((100.0/self.urlResponse.expectedContentLength)*self.downloadedMutableData.length)/100;
    if (self.m_downProgressView.progress == 1)
    {
        [self dismissDownloadPopup];
    }
    
    self.m_lbDownProgress.text = [NSString stringWithFormat:@"%.0f%%", ((100.0/self.urlResponse.expectedContentLength)*self.downloadedMutableData.length)];
    NSLog(@"%.0f%%", ((100.0/self.urlResponse.expectedContentLength)*self.downloadedMutableData.length));
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (self.downloadedMutableData)
    {
        NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, sDownloadFileName];
        
        //saving is done on main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.downloadedMutableData writeToFile:filePath atomically:YES];
            NSLog(@"File Saved to %@!", filePath);
            
            [self saveToDownloadedList];
            [self.m_tableView reloadData];
        });
    }
    else
    {
        NSLog(@"Download Failed.");
    }
}

-(void)saveToDownloadedList
{
    MyList* g_list = [MyList getInstance];
    if(g_list.m_downloadedList == nil)
        g_list.m_downloadedList = [[NSMutableArray alloc] init];
    
    [g_list.m_downloadedList addObject:[self getVideoData:nDownloadIndex]];
}

-(VideoData*)getVideoData:(NSInteger)index
{
    NSDictionary* dic = [resultString objectAtIndex:index];
    
    VideoData* data = [[VideoData alloc] init];
    data.video_id = [dic objectForKey:@"VideoID"];
    data.title = [dic objectForKey:@"VideoTitle"];
    data.artist = [dic objectForKey:@"VideoArtist"];
    data.mp4_name = [dic objectForKey:@"MP4Name"];
    data.publish_date = [dic objectForKey:@"PublishDate"];
    data.previewID = [dic objectForKey:@"YouTubePreviewID"];
    data.instructor = [dic objectForKey:@"InstructorID"];
    data.song_key = [dic objectForKey:@"SongKey"];
    data.video_length = [dic objectForKey:@"VideoRuntime"];
    
    return data;
}

- (IBAction)onDownloadCancel:(UIButton *)sender {
    [self cancelDownload];
}

-(void)cancelDownload{
    [self.connectionManager cancel];
    self.connectionManager = nil;
    self.downloadedMutableData = nil;
    [self dismissDownloadPopup];
}

-(void)dismissDownloadPopup{
    self.m_ProgressPopup.hidden = YES;
    [self disableBackground:NO];
}

#pragma mark GADRewardBasedVideoAdDelegate implementation

- (void)rewardBasedVideoAdDidReceiveAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is received.");
}

- (void)rewardBasedVideoAdDidOpen:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Opened reward based video ad.");
}

- (void)rewardBasedVideoAdDidStartPlaying:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad started playing.");
    
    
}

- (void)rewardBasedVideoAdDidClose:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is closed.");
    
    if (![[GADRewardBasedVideoAd sharedInstance] isReady]) {
        [self requestRewardedVideo];
    }
    
    if (videoWatched == true)
    {
        videoWatched = false;
        
        
        m_currentVideoURL = [NSURL URLWithString:[NSString stringWithFormat:DOWNLOAD_URL, mp4_full]];
        
        NSLog(@"vurl %@",m_currentVideoURL);
        
        [m_loader startAnimation];
        [self disableBackground:YES];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startPlay) name:@"ValidFileNotification" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(alertVideo) name:@"InValidFileNotification" object:nil];
        
        if(checkObj == nil)
            checkObj = [[CheckFileUrl alloc] init];

        [checkObj checkUrl:m_currentVideoURL];
       
    }
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
   didRewardUserWithReward:(GADAdReward *)reward {
    NSString *rewardMessage =
    [NSString stringWithFormat:@"Reward received with currency %@ , amount %lf", reward.type,
     [reward.amount doubleValue]];
    NSLog(@"%@", rewardMessage);
    videoWatched = true;
    // Reward the user for watching the video.
}

- (void)rewardBasedVideoAdWillLeaveApplication:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad will leave application.");
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
    didFailToLoadWithError:(NSError *)error {
    NSLog(@"Reward based video ad failed to load.");
}


- (void)safariViewControllerDidFinish:(SFSafariViewController *)controller {
    [self dismissViewControllerAnimated:true completion:nil];
}
- (IBAction)ImgeUrl_Action:(id)sender
{
    //    SFSafariViewController *svc = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString: @"http://www.organclubhouse.com/w/membership-benefits/"]];
    //    svc.delegate = self;
    //    [self presentViewController:svc animated:YES completion:nil];
    //
    //    UIApplication *application = [UIApplication sharedApplication];
    //    [application openURL:[NSURL URLWithString: @"http://www.organclubhouse.com/w/membership-benefits/"] options:@{} completionHandler:nil];
    
    
    
    //fade in
    [UIView animateWithDuration:1.0f animations:^{
        _alpha_view.hidden=NO;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:2.0f animations:^{
            //            [_alpha_View setAlpha:0.0f];
            _alpha_view.hidden=NO;
        } completion:nil];
    }];
    
    _Popup_view.hidden=NO;
    _Popup_view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    //Activity_Indicator
    
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        
        _Popup_view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
        
    }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.3/2 animations:^{
                             
                             _Popup_view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                             
                         }
                                          completion:^(BOOL finished) {
                                              
                                              [UIView animateWithDuration:0.3/2 animations:^{
                                                  _Popup_view.transform = CGAffineTransformIdentity;
                                                  
                                              }];
                                          }];
                     }];
}

- (IBAction)Sunscribe_today:(id)sender

{
    if ([SFSafariViewController class] != nil) {
        SFSafariViewController *safariViewController = [[SFSafariViewController alloc]initWithURL:[NSURL URLWithString:@"http://www.organclubhouse.com/w/membership-benefits//"]];
        safariViewController.delegate = self;
        [self presentViewController:safariViewController animated:true completion:nil];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.organclubhouse.com/w/membership-benefits/"]];
    }
    
    
    
}

- (IBAction)close_Action:(id)sender
{
    _Popup_view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    _Popup_view.hidden=YES;
    _alpha_view.hidden=YES;
    
    
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        
        _Popup_view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
        
    }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.3/2 animations:^{
                             
                             _Popup_view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                             
                         }                     completion:^(BOOL finished) {
                             
                             [UIView animateWithDuration:0.3/2 animations:^{
                                 _Popup_view.transform = CGAffineTransformIdentity;
                                 
                             }];
                         }];
                     }];
    
}


- (IBAction)rating_itunesLink:(id)sender
{
    [[NSUserDefaults standardUserDefaults]setInteger:555 forKey:@"rateUs"];
    self.view_rating_backView.hidden = true;
    self.view11.hidden = true;
    NSString *iTunesLink = @"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=1238735500&pageNumber=0&sortOrdering=1&type=Purple+Software&mt=8";         //  @"https://itunes.apple.com/in/app/pianoclubhouse-easy-piano-song-tutorial-lessons/id1238735500?mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

- (IBAction)rating_notNow:(id)sender
{
    _view11.hidden = true;
    _view_rating_backView.hidden = true;
    [[NSUserDefaults standardUserDefaults]setInteger:555 forKey:@"rateUs"];
}



@end
