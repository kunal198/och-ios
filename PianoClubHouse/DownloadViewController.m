//
//  FavoriteViewController.m
//  PianoClubHouse
//
//  Created by kingcode on 8/16/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "DownloadViewController.h"
#import "ListCell.h"
#import "G_Functions.h"
#import "Common.h"
#import "KxMenu.h"
#import "FakeModelBuilder.h"
#import "MyList.h"
#import "G_Filters.h"
#import "VideoData.h"
#import "PCHMaskView.h"
#import "UIButton+Tagged.h"

@import MediaPlayer;

@interface DownloadViewController ()
{
    BOOL m_bTitleAscending;
    BOOL m_bArtistAscending;
    BOOL m_bDateAscending;
    
    NSMutableArray* arrData;
    
    NSString* video_id;
    NSString* mp4_name;
    
    NSInteger m_nCount;
    CGRect m_rtPortrait;
    
    BOOL bAllowRotate;
}

@end

@implementation DownloadViewController

@synthesize m_tableView;
@synthesize m_navBar;
@synthesize m_sortType;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]];
    
    if(IDIOM == IPAD)
        m_navBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar_ipad"]];
    else
        m_navBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"browse_title"]];
    
    m_bTitleAscending = NO;
    m_bArtistAscending = NO;
    m_bDateAscending = NO;
    
    if(![G_Functions isPremiumMode])
        self.m_imgTableBack.image = [UIImage imageNamed:@"table_back_pub"];
    
    m_rtPortrait = self.view.frame;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceOrientationPortrait) name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];
}

- (BOOL)shouldAutorotate
{
    return bAllowRotate;
}

- (void)forceOrientationPortrait
{
    bAllowRotate = YES;
    
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInt:UIInterfaceOrientationPortrait] forKey:@"orientation"];
    
    bAllowRotate = NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    
    if (self.transitionCoordinator && self.presentedViewController.presentationController)
    {
        
        CGRect rect = self.presentedViewController.presentationController.containerView.bounds;
        self.view.frame = rect;
        //CGRect rect = CGRectMake(0, 0, 320, 568);
        //self.view.frame = CGRectMake(10, 20, 320, 568);
        NSNumber* value = [NSNumber numberWithInt:UIInterfaceOrientationMaskLandscapeLeft];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
        
    }
    
    MyList* g_list = [MyList getInstance];
    arrData = g_list.m_downloadedList;
    
    m_nCount = [arrData count];
    [self updateCount];
    
    [self tableRefresh];
    
    [super viewWillAppear:animated];
    
}

-(void)updateCount{
    
    self.m_sortType.text = [NSString stringWithFormat:@"%lu videos, sorted by publish date", (unsigned long)m_nCount];
}

-(void)tableRefresh
{
    [self menu:arrData];
    [m_tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

-(NSArray *)menu:(NSMutableArray*)array  {
    
    _menu = [FakeModelBuilder buildFavoriteListMenu:array];
    return _menu;
}

#pragma mark - RRNCollapsableTableView

-(NSString *)sectionHeaderNibName
{
    if([G_Functions isPremiumMode])
        return IDIOM == IPAD ? @"MenuSectionHeaderView_iPad" : @"MenuSectionHeaderView";
    else
        return @"MenuSectionHeaderView1";
}

-(NSArray *)model {
    return self.menu;
}

-(UITableView *)collapsableTableView {
    return m_tableView;
}


-(void)sortTableDataUsingSortKey:(NSString *)sortKey withAscending:(BOOL)isAscending
{
    NSArray *sortDescriptors = [NSArray arrayWithObject:[[NSSortDescriptor alloc]initWithKey:sortKey ascending:isAscending]];
    NSArray *arrTemp = [[NSArray alloc] initWithArray:[arrData sortedArrayUsingDescriptors:sortDescriptors]];
    [arrData count]?[arrData removeAllObjects]:NSLog(@"Datasource Table Not null");
    [arrData addObjectsFromArray:arrTemp];
    arrTemp = nil;
    
    [self tableRefresh];
}


#pragma mark - UITableView

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return IDIOM == IPAD? 86.0f : 45.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self model].count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellIdentifier = @"listCell";
    ListCell* cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(!cell)
    {
        cell = [[ListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"detail_bg"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0]];
    VideoData* data = [arrData objectAtIndex:indexPath.section];
    
    cell.video_id = data.video_id;  [cell refreshFavorites];
    cell.title = data.title;
    cell.artist = data.artist;
    cell.preview_id = data.previewID;
    cell.m_lbVideoLength.text = data.video_length;
    cell.m_lbPublishDate.text = data.publish_date;
    cell.m_lbSongKey.text = data.song_key;
    cell.m_lbInstructor.text = data.instructor;
    cell.mp4_name = data.mp4_name;
    
    cell.m_deleteBtn.tag = [data.video_id intValue];
    cell.m_deleteBtn.stringTag = data.mp4_name;
    cell.m_btnWatch.stringTag = data.mp4_name;
    cell.m_btnWatchOffline.stringTag = data.mp4_name;
    cell.m_lbl_Artist.text = data.artist;
    
    return cell;
}

-(NSString*)getSongKey:(NSString*)keyID
{
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *myNumber = [f numberFromString:keyID];
    
    G_Filters *gFilters = [G_Filters getInstance];
    NSString *songKey = [gFilters.m_keyFilters objectForKey:myNumber];
    return songKey;
}

-(NSString*)getInstructor:(NSString*)instructorID
{
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *myNumber = [f numberFromString:instructorID];
    
    int value = [myNumber intValue];
    myNumber = [NSNumber numberWithInt:value - 1];
    
    G_Filters *gFilters = [G_Filters getInstance];
    NSString *instructor = [gFilters.m_instructorFilters objectForKey:myNumber];
    return instructor;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    if ([m_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [m_tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([m_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [m_tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma marks - DateFormatter

-(NSString*)convertDateFormat:(NSString*)str
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *date = [dateFormatter dateFromString: str];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yy"];
    
    NSString *convertedString = [dateFormatter stringFromDate:date];
    NSLog(@"Converted String : %@",convertedString);
    
    return convertedString;
}


#pragma marks - Sort

- (IBAction)onSortBtnClick:(id)sender {
    [self showMenu:sender];
}

- (IBAction)onRemoveDownload:(id)sender {
    
    video_id  = [NSString stringWithFormat:@"%li", (long)((UIControl*)sender).tag];
    mp4_name = ((UIButton*)sender).stringTag;
    
    UIAlertView *confirmDlg=[[UIAlertView alloc]initWithTitle:@"OrganClubHouse" message:@"Confirm removal of this download?" delegate:self
                                            cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [confirmDlg show];
}

-(void)removeFromDownloadedList
{
    MyList* g_list = [MyList getInstance];
    for (int i=0;i<[g_list.m_downloadedList count]; i++) {
        VideoData *item = [g_list.m_downloadedList objectAtIndex:i];
        if ([item.video_id isEqualToString:video_id]) {
            [g_list.m_downloadedList removeObject:item];
            i--;
            arrData = g_list.m_downloadedList;
            m_nCount = [arrData count];
            [self updateCount];
            return;
        }
    }
}

-(void)deleteVideo
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:mp4_name];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        UIAlertView *removeSuccessFulAlert=[[UIAlertView alloc]initWithTitle:@"OrganClubHouse" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [removeSuccessFulAlert show];
        [self removeFromDownloadedList];
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
    
    [self tableRefresh];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex) {
        case 0: //"No" pressed
            break;
        case 1: //"Yes" pressed
        {
            [self deleteVideo];
            break;
        }
    }
}

- (IBAction)onWatchOfflineBtnClick:(id)sender
{
    NSString* mp4Name  = ((UIButton*)sender).stringTag;
    NSURL *videoURL;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString* filePath = [documentsDirectory stringByAppendingPathComponent:mp4Name];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    if(!fileExists)
    {
        NSLog(@"=====No file====");
        return;
    }
    
    videoURL = [NSURL fileURLWithPath:filePath];
    NSLog(@"vurl %@",videoURL);
    MPMoviePlayerViewController *videoPlayerView = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
    [self presentMoviePlayerViewControllerAnimated:videoPlayerView];    
    [videoPlayerView.moviePlayer play];
}

- (void)showMenu:(UIButton *)sender
{
    NSArray *menuItems =
    @[
      
      [KxMenuItem menuItem:@"Sort By Title"
                     image:nil
                    target:self
                    action:@selector(sortByTitle)],
      
      [KxMenuItem menuItem:@"Sort By Artist"
                     image:nil
                    target:self
                    action:@selector(sortByArtist)],
      
      [KxMenuItem menuItem:@"Sort By Date"
                     image:nil
                    target:self
                    action:@selector(sortByDate)],
      ];
    
    [KxMenu showMenuInView:self.view
                  fromRect:sender.frame
                 menuItems:menuItems];
}

-(void)sortByTitle
{
    m_bTitleAscending = !m_bTitleAscending;
    self.m_sortType.text = [NSString stringWithFormat:@"%lu videos, sorted by title", (unsigned long)m_nCount];
    [self sortTableDataUsingSortKey:@"title" withAscending:m_bTitleAscending];
}

-(void)sortByArtist
{
    m_bArtistAscending = !m_bArtistAscending;
    self.m_sortType.text = [NSString stringWithFormat:@"%lu videos, sorted by artist", (unsigned long)m_nCount];
    [self sortTableDataUsingSortKey:@"artist" withAscending:m_bArtistAscending];
}

-(void)sortByDate
{
    m_bDateAscending = !m_bDateAscending;
    self.m_sortType.text = [NSString stringWithFormat:@"%lu videos, sorted by publish date", (unsigned long)m_nCount];
    [self sortTableDataUsingSortKey:@"publish_date" withAscending:m_bDateAscending];
}

@end
