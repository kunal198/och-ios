//
//  LoginViewController.h
//  PianoClubHouse
//
//  Created by kingcode on 9/5/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *update_alert;
- (IBAction)update_val:(id)sender;

- (IBAction)onBecomeBtnClick:(id)sender;
- (IBAction)onLoginBtnClick:(id)sender;
- (IBAction)onGuestBtnClick:(id)sender; 
- (IBAction)onRememberBtnClick:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *m_txtName;
@property (weak, nonatomic) IBOutlet UITextField *m_txtPwd;
@property (strong, nonatomic) IBOutlet UIView *welcomView;
@property (strong, nonatomic) IBOutlet UIView *loginPasswordView;

@end
