//
//  InstructorFilterViewController.m
//  PianoClubHouse
//
//  Created by kingcode on 8/22/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "InstructorFilterViewController.h"
#import "FilterInstructorCell.h"
#import "Common.h"
#import "SearchViewController.h"
#import "G_Functions.h"

@interface InstructorFilterViewController (){
    
}
@end

@implementation InstructorFilterViewController

@synthesize m_tableView;
@synthesize m_arrInstructors;
@synthesize m_arrSelected;
@synthesize m_navbar;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   // self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"search_back"]];
    
    if(IDIOM == IPAD)
        m_navbar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar_ipad"]];
    else
        m_navbar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"browse_title"]];
    
    m_tableView.allowsSelection = NO;
    m_arrSelected = appdelegate.m_arrSelectedInstructors;
    [self initFilters];
}

- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)initFilters
{
    m_arrInstructors = [[NSMutableArray alloc]initWithObjects:
                        @"Carlton Whitfield",
                       
                        @"Jermaine Roberts",
                        nil];
    
    
 /*   m_arrInstructors = [[NSMutableArray alloc]initWithObjects:@"Tj Hanes",
                                                             @"Carlton “CDub” Whitfield",
                                                             @"Darrell Cook",
                                                             @"Shaun Martin",
                                                             @"Jermaine Roberts",
                                                             nil];*/
}

- (IBAction)onBackBtnClick:(id)sender
{
    //[self getSelectedInstructors];
    
    SearchViewController* pBackView = (SearchViewController*)[self backViewController];
    pBackView.m_instructorFilters = m_arrSelected;
    NSLog(@"%@",m_arrSelected);
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIViewController *)backViewController
{
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers < 2)
        return nil;
    else
        return [self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 2];
}

#pragma marks - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return m_arrInstructors.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   // if(indexPath.row == 6)
   //     return 0.0;
    
    return IDIOM == IPAD? 117.0f : 60.0f;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellIdentifier = @"filterCell";
    FilterInstructorCell* cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell)
    {
        cell = [[FilterInstructorCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.m_toggleBtn.tag = indexPath.row;
    /*
    if(indexPath.row == 6)
        cell.hidden = YES;
    */
   /* if([G_Functions isPremiumMode])
        cell.filterName.textColor  = Rgb2UIColor(255,255,255);//(128, 131, 140);
    else
        cell.filterName.textColor  = Rgb2UIColor(0,0,0);
    */
    cell.filterName.text = [m_arrInstructors objectAtIndex:indexPath.row];
    
    NSNumber *i = [[NSNumber alloc] initWithInteger:indexPath.row];
    if(m_arrSelected && ([m_arrSelected indexOfObject:i] != NSNotFound))
        cell.m_toggleBtn.selected = YES;
    else
        cell.m_toggleBtn.selected = NO;
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    if ([m_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [m_tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([m_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [m_tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma marks - Button Actions

-(void)getSelectedInstructors
{
    NSInteger sections = m_tableView.numberOfSections;
    for (int section = 0; section < sections; section++) {
        NSInteger rows =  [m_tableView numberOfRowsInSection:section];
        for (int row = 0; row < rows; row++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            FilterInstructorCell*cell = (FilterInstructorCell*)[self.m_tableView cellForRowAtIndexPath:indexPath];//**here, for those cells not in current screen, cell is nil**
            if(m_arrSelected == nil)
                m_arrSelected = [[NSMutableArray alloc]init];
            if(cell.m_toggleBtn.selected)
            {
                if([m_arrSelected indexOfObject:[NSNumber numberWithInteger:indexPath.row]] == NSNotFound)
                    [m_arrSelected addObject:[NSNumber numberWithInteger:indexPath.row]];
            }
            else
            {
                if([m_arrSelected indexOfObject:[NSNumber numberWithInteger:indexPath.row]] != NSNotFound)
                    [m_arrSelected removeObject:[NSNumber numberWithInteger:indexPath.row]];
            }
        }
    }
}

- (IBAction)onResetBtnClick:(id)sender {
    
    NSInteger sections = m_tableView.numberOfSections;
    for (int section = 0; section < sections; section++) {
        NSInteger rows =  [m_tableView numberOfRowsInSection:section];
        for (int row = 0; row < rows; row++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            FilterInstructorCell*cell = (FilterInstructorCell*)[self.m_tableView cellForRowAtIndexPath:indexPath];//**here, for those cells not in current screen, cell is nil**
            [cell resetToggleBtn];
        }
    }
    
    [m_arrSelected removeAllObjects];
}


@end
