

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "MKStoreObserver.h"
#import "AppDelegate.h"

@interface MKStoreManager : NSObject<SKProductsRequestDelegate> {
    
	NSMutableArray *purchasableObjects;
	MKStoreObserver *storeObserver;

}

@property (nonatomic, retain) NSMutableArray *purchasableObjects;
@property (nonatomic, retain) MKStoreObserver *storeObserver;

- (void) requestProductData;

- (void) buyVideo; // expose product buying functions, do not expose

// do not call this directly. This is like a private method
- (void) buyVideo:(NSString*) featureId;

- (void) failedTransaction: (SKPaymentTransaction *)transaction;

+ (MKStoreManager*)sharedManager;

+ (BOOL) videoPurchased;

+(void) restorePurchases;
+(void) loadPurchases;
+(void) updatePurchases;
-(void) setSender:(id)sender;

@end
