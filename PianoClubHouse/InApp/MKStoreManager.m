

#import "MKStoreManager.h"


#import "AppDelegate.h"

@implementation MKStoreManager

@synthesize purchasableObjects;
@synthesize storeObserver;

// all your features should be managed one and only by StoreManager
static NSString *g_productId = @"com.pianoclubhouse.videotutorials";   //  - consumable


BOOL videoPurchased;

static MKStoreManager* _sharedStoreManager; // self

+ (BOOL) videoPurchased {
	return videoPurchased;
}

+(void)dealloc
{
    
}

+ (MKStoreManager*)sharedManager
{
	NSLog(@"pass sharedManager");
    //	@synchronized(self) { // SKY:130515
    
    if (_sharedStoreManager == nil) {
        
        [[self alloc] init]; // assignment not done here
        _sharedStoreManager.purchasableObjects = [[NSMutableArray alloc] init];
        //[_sharedStoreManager requestProductData];
        
        [MKStoreManager loadPurchases];
        _sharedStoreManager.storeObserver = [[MKStoreObserver alloc] init];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:_sharedStoreManager.storeObserver];
    }
    //    }
    return _sharedStoreManager;
}


#pragma mark Singleton Methods

+ (id)allocWithZone:(NSZone *)zone

{
    @synchronized(self) {
		
        if (_sharedStoreManager == nil) {
			
            _sharedStoreManager = [super allocWithZone:zone];
            return _sharedStoreManager;  // assignment and return on first allocation
        }
    }
	
    return nil; //on subsequent allocation attempts return nil
}


- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (void) requestProductData
{
	SKProductsRequest *request= [[SKProductsRequest alloc] initWithProductIdentifiers:
								 [NSSet setWithObjects:
                                  g_productId,
								  nil]]; // add any other product here
	request.delegate = self;
	[request start];
}


- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
	[purchasableObjects addObjectsFromArray:response.products];
	// populate your UI Controls here
	for(int i=0;i<[purchasableObjects count];i++)
	{
		SKProduct *product = [purchasableObjects objectAtIndex:i];
		NSLog(@"Feature: %@, Cost: %f, ID: %@",[product localizedTitle],
			  [[product price] doubleValue], [product productIdentifier]);
	}
	
	//[request autorelease];
}

- (void) buyVideo {
    
	[self buyVideo:g_productId];
}

- (void) buyVideo:(NSString*) featureId
{
    NSLog(@"%d",[self.purchasableObjects count]);
    [SKPaymentQueue canMakePayments];
    //[self provideContent:feature0Id];
	if ([SKPaymentQueue canMakePayments] && [self.purchasableObjects count] > 0 )
	{
		SKPayment *payment = [SKPayment paymentWithProductIdentifier:featureId];
		[[SKPaymentQueue defaultQueue] addPayment:payment];
        

	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OrganClubHouse" message:@"You are not authorized to purchase from AppStore" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
	}
    
}

- (void) failedTransaction: (SKPaymentTransaction *)transaction
{
	NSString *messageToBeShown = [NSString stringWithFormat:@"Reason: %@, You can try: %@", [transaction.error localizedFailureReason], [transaction.error localizedRecoverySuggestion]];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to complete your purchase" message:messageToBeShown delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];
	//[alert release];
}

+(void) loadPurchases
{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	videoPurchased = [userDefaults boolForKey:g_productId];
}

+(void) updatePurchases
{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setBool:videoPurchased forKey:g_productId];
    [userDefaults synchronize];
}

+(void) restorePurchases
{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:_sharedStoreManager.storeObserver];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

-(void) setSender:(id)sender
{
    [_sharedStoreManager.storeObserver setSender:sender];
}

@end
