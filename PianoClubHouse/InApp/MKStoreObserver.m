//
//  MKStoreObserver.m
//
//

#import "MKStoreObserver.h"
#import "MKStoreManager.h"

@implementation MKStoreObserver

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
	for (SKPaymentTransaction *transaction in transactions)
	{
		switch (transaction.transactionState)
		{
			case SKPaymentTransactionStatePurchased:
            {
                [self completeTransaction:transaction];
                ListCell* sender = (ListCell*)m_sender;
                [sender saveToPurchasedList];
                [sender refresh];
                break;
            }
            case SKPaymentTransactionStateFailed:
				
                [self failedTransaction:transaction];
                //[GameSettings setHints:[GameSettings nHints] + 5];
				//[[MKStoreManager sharedManager] provideContent: transaction.payment.productIdentifier];
                break;
				
            case SKPaymentTransactionStateRestored:
				
                [self restoreTransaction:transaction];
				
            default:
				
                break;
		}
	}
}

- (void) failedTransaction: (SKPaymentTransaction *)transaction
{
    if (transaction.error.code != SKErrorPaymentCancelled){
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"The upgrade procedure failed" message:@"Please check your Internet connection and your App Store account information." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		//[alert release];
	}
	
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void) completeTransaction: (SKPaymentTransaction *)transaction
{
    
//    [[MKStoreManager sharedManager] provideContent: transaction.payment.productIdentifier];
//	//[[MKStoreManager sharedManager] setLockKey: transaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void) restoreTransaction: (SKPaymentTransaction *)transaction
{
//    [[MKStoreManager sharedManager] provideContent: transaction.originalTransaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void) setSender: (id)sender
{
    m_sender = sender;
}

@end
