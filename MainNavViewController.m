//
//  MainNavViewController.m
//  PianoClubHouse
//
//  Created by kingcode on 9/4/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "MainNavViewController.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "Common.h"

@interface MainNavViewController ()

@end

@implementation MainNavViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prefersStatusBarHidden];
//    if(IDIOM == IPAD)
//        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"icon"]];
//    else
//        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"icon"]];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [self performSegueWithIdentifier:@"downloadSegue" sender:self];
        NSLog(@"There is no internet connection");
    }
    else
    {
        if([self checkLoginedState]){
            
            AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.appMode = PREMIUM;
            [self performSegueWithIdentifier:@"skipToMain" sender:self];         
        }
        else{
            
            [self performSegueWithIdentifier:@"mainSegue" sender:self];
        }
        
        NSLog(@"There is internet connection");
    }
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}

- (BOOL)checkLoginedState
{
    NSString *sFullName = [[NSUserDefaults standardUserDefaults] stringForKey:@"FullName"];
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.g_UserName = sFullName;
    
    NSString *sName = [[NSUserDefaults standardUserDefaults] stringForKey:@"Name"];
    NSString *sPwd = [[NSUserDefaults standardUserDefaults] stringForKey:@"PWd"];
    if(sName == nil || sPwd == nil)
        return NO;
    
    NSString *sDate = [[NSUserDefaults standardUserDefaults] stringForKey:@"Date"];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormat setTimeZone:timeZone];
    NSDate *prev_Date = [dateFormat dateFromString:sDate];
    NSDate *today = [NSDate date];
    
    NSTimeInterval secondsBetween = [today timeIntervalSinceDate:prev_Date];
    
    int numberOfDays = secondsBetween / 86400;
    NSLog(@"There are %d days in between the two dates.", numberOfDays);
    
    if(numberOfDays >= 3){
     
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"FullName"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Name"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Pwd"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Date"];
        return NO;
    }
    
    return YES;
    
}


@end
