//
//  WarningViewController.m
//  PianoClubHouse
//
//  Created by mrinal khullar on 4/20/17.
//  Copyright © 2017 kingcode. All rights reserved.
//

#import "WarningViewController.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "Reachability.h"
#import "MyList.h"
#import "Common.h"
#import "Reachability.h"
#import "MainViewController.h"

@interface WarningViewController ()
{
    UIButton *button;

}
@end

@implementation WarningViewController
NSString * updateDetail;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.splashScreen.frame = CGRectMake(self.view.frame.origin.x - self.splashScreen.frame.size.width, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    NSString *appVersion = [[NSUserDefaults standardUserDefaults]objectForKey:@"updateDetail_key"];
    
    if ( [appVersion isEqualToString:@"update"]) {
        
        if([[[NSUserDefaults standardUserDefaults]objectForKey:@"AppUnderMaintenance"] isEqualToString:@"YES"]){
            
            UILabel *l =[self.view viewWithTag:1];
            [l setNumberOfLines:5];
            [l setText:@"We are sorry, but the app is currently undergoing routine maintenance. Please check back at a later time"];
            button = [self.view viewWithTag:2] ;
            [button setTitle:@"Check Later" forState:UIControlStateNormal];
        }
        
        else{
            UILabel *l =[self.view viewWithTag:1];
            [l setText:@"Good news! A new version of the app is available."];
            
        }
    }
    
    
}

- (IBAction)buttonPressed:(id)sender {
    
    if([button.titleLabel.text isEqualToString:@"Check Later"])
    {
       // exit(0)
        NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
        NSString* appID = infoDictionary[@"CFBundleIdentifier"];
        NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.organclubhouse.com/api/?option=get_version"]];
        
        NSData* data = [NSData dataWithContentsOfURL:url];
        NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSString* maintenance_mode = lookup[@"maintaince_mode"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"AppUnderMaintenance"];
        
        if([maintenance_mode isEqualToString:@"on"])
        {
            updateDetail = @"update";
            [[NSUserDefaults standardUserDefaults]setObject:updateDetail forKey:@"updateDetail_key"];
            [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"AppUnderMaintenance"];
            // animation 1
            [UIView animateWithDuration:0.75 delay:0.0
                                options:optind
                             animations:^{
                                 _splashScreen.frame = CGRectMake(_splashScreen.frame.origin.x /*+ self.splashScreen.frame.size.width*/, _splashScreen.frame.origin.y, _splashScreen.frame.size.width, _splashScreen.frame.size.height);
                                 self.view.frame = CGRectMake(self.view.frame.origin.x + self.splashScreen.frame.size.width, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
                             }
                             completion:^(BOOL finished) {
                                 [UIView animateWithDuration:0.5 delay:0.5
                                                     options:optind
                                                  animations:^{
                                                      _splashScreen.frame = CGRectMake(_splashScreen.frame.origin.x/* - self.splashScreen.frame.size.width*/, _splashScreen.frame.origin.y, _splashScreen.frame.size.width, _splashScreen.frame.size.height);
                                                      
                                                      self.view.frame = CGRectMake(self.view.frame.origin.x - self.splashScreen.frame.size.width, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
                                                  }
                                                  completion:^(BOOL finished) {
                                                  }];
                             }];
        }
        else
        {
            
            UIStoryboard *mainStoryboard;
            if(IDIOM == IPAD){
                mainStoryboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle: nil];
            }
            else{
                mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            }
           
            
            LoginViewController * login = (LoginViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"login_view"];
            [self presentViewController:login animated:YES completion:nil];

        }
        
    }
    else{
        if([button.titleLabel.text isEqualToString:@"Check Later"])
        {
            NSString *iTunesLink = @"https://itunes.apple.com/in/app/pianoclubhouse-premium/id1063098413?mt=8";
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
        }
        else
        {
            NSString *iTunesLink = @"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=1238735500&pageNumber=0&sortOrdering=1&type=Purple+Software&mt=8";
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
        
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
