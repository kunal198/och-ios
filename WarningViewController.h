//
//  WarningViewController.h
//  PianoClubHouse
//
//  Created by mrinal khullar on 4/20/17.
//  Copyright © 2017 kingcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WarningViewController : UIViewController
@property (strong, nonatomic) UIWindow *window;


@property (strong, nonatomic) IBOutlet UIView *splashScreen;
@property (strong, nonatomic) IBOutlet UIImageView *splashScreenImage;

@end
